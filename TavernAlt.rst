Tavern (lihtsam variant)
========================

Kaust Gitis: ``EX04TavernAlt``

Pakk: ``ee.ttu.iti0202.tavern``

Ülesanne on lihtsustatud versioon EX04Tavern ülesandest. Selles variandis puuduvad klassid ``Currency`` ja ``Price``. Ülejäänud funktsionaalsus on suures osas sama.

Soovitatav on vaadata ka EX04 Tavern ülesannet.

Sellel ülesandel ei ole boonusosa. Kokku on võimalik saada 1p. EX04 Tavern ülesande puhul on umbes sama funktsionaalsuse eest võimalik saada 1.5p.

Punkte saab vaid ühe ülesande eest: kas EX04Tavern või EX04TavernAlt. Arvesse läheb see, mis rohkem annab.

Kui see ülesanne on lahendatud, siis siit EX04Tavern juurde ei ole väga raske liikuda. Peamine on, et põhiülesande puhul tuleb aru saada
``Currency`` ja ``Price`` klassidest - ülejäänud on umbes sama.

Ülesandes on kasutusel kolm valuutat: "copper", "silver", "gold". Testides kasutatakse vaid neid valuutasid. Programm ei pea valideerima,
kas sisestatud valuuta on üks neist.

Valuutade omavaheline kurss: 1 silver == 10 copper. 1 gold == 100 copper.

Klass ``Coin``
--------------

Tähistab ühte münti, millel on valuuta. (Erinevalt põhiülesandega, mündil väärtust ei saagi olla.)

Meetodid:

- ``public Coin(String currency)`` - loob uue mündi, mille valuuta on ``currency``.
- ``public int getRate()`` - tagastab mündi väärtuse baasvaluutas (copper -> 1, silver -> 10, gold -> 100)

Tuleb üle kirjutada järgised meetodid (``@Override``):

- ``public boolean equals(Object o)`` - tagastab true juhul, kui kahel mündil on sama valuuta. Seoses sellega peaks uuendama ka ``hashCode()`` meetodit.
- ``public String toString()`` - tagastab valuuta nimetuse

Klass ``Purse``
---------------

Tähistab rahakotti, mis koosneb müntidest.

Meetodid:

- ``public Purse(Coin... coins)`` - konstruktor. Võtab argumendina sisse mitu münti, mis lisatakse rahakotti. Vt näiteid alt.
- ``public void addCoin(Coin coin)`` - lisab mündi rahakotti.
- ``public List<Coin> getCoins()`` - tagastab rahakoti kõik mündid.
- ``public List<Coin> pay(int price)`` - maksab võimalusel hinna. Kui rahakotis olevate müntidega saab hinda tasuda, tuleb see tasuda.
  Meetod võib maksta ka rohkem (näiteks kui on kuldmünt, siis sellega võib tasuda näiteks hinna 96).
  Kui rahakotis olevate müntidega ei õnnestu hinda tasuda, tagastab ``null``. Muul juhul tagastab mündid, mis tasumiseks kuluvad.
  Need mündid tuleb rahakotist ka eemaldada.

Klass ``Tavern``
----------------

Tähistab kõrtsi, kust saab süüa (ja juua) osta. Toidu hind on alati mittenegatiivna.

Meetodid:

- ``public void addFood(String name, int price)`` - lisatakse toit nimega ``name`` ja hinnaga ``price``. Ühe nimega tohib olla vaid üks toit (üks hind).
- ``public int getPriceForFood(String name)`` - tagastab toidu, mille nimi on ``name``, hinna. Kui sellist toitu pole, tagastab -1.
- ``public Map<String, Integer> getPriceMap(String name)`` - tagastab toidu, mille nimi on ``name``, hinna kujutisena (*map*).
  Kujutise võti on valuuta nimi, väärtus on vastava valuuta kogus.
  Tagastab optimaalseimal kujul hinna erinevates valuutades. See tähendab seda, et hind 123 peaks siit tagasi tulema gold: 1, silver: 2, copper: 3.
- ``public String getPriceString(String name)`` - nagu ``getPriceMap()``, tagastab hinna optimaalseimal kujul, aga sõnena. 123 => "1 gold, 2 silver, 3 copper".
  Kui mõnda osa pole vaja kasutada, tuleb see ära jätta. 104 => "1 gold, 4 copper". Esimesel kohal on kuld, siis hõbe, siis vask.
- ``public boolean buy(String name, Purse purse)`` - kõrtsist ostetakse toitu. Kui kõrtsis pole vastava nimetusega toitu või
  rahakotis pole piisavalt raha, tagastab meetod ``false``. Muul juhul tagastab ``true``.
  Peale edukat ostmist saab nimetatud toit kõrtsist otsa (sama nimega toitu enam osta ei saa).
  Samuti väheneb peale edukat ostmist rahakoti sisu.
  Peaks kasutama ``purse.pay()`` meetodit.

Näide
-----

.. code-block:: java

    Coin c1 = new Coin("copper");
    Coin c2 = new Coin("silver");
    Coin c3 = new Coin("gold");

    System.out.println(c1.getCurrency()); // copper
    System.out.println(c1.getRate());     // 1
    System.out.println(c2.getCurrency()); // silver
    System.out.println(c2.getRate());     // 10
    System.out.println(c3.getCurrency()); // gold
    System.out.println(c3.getRate());     // 100

    Purse purse = new Purse(new Coin("copper"), new Coin("silver"), new Coin("gold"));
    System.out.println(purse.getCoins());
    System.out.println(purse.pay(112)); // null <- cannot pay the price
    System.out.println(purse.pay(111)); // [gold, silver, copper]

    Tavern t = new Tavern();
    t.addFood("Milk", 13);
    System.out.println(t.getPriceForFood("Milk"));      // 13
    System.out.println(t.getPriceMap("Milk"));          // {copper=3, silver=1}
    System.out.println(t.getPriceForFood("Cheese"));    // -1
    System.out.println(t.getPriceMap("Cheese"));        // null

    purse.addCoin(new Coin("gold"));
    purse.addCoin(new Coin("gold"));
    System.out.println(t.buy("Milk", purse));  // true
    System.out.println(t.buy("Milk", purse));  // false  <- no more milk for ya

    purse = new Purse(new Coin("silver"));
    t.addFood("Beer", 11);
    t.addFood("Small beer", 10);
    System.out.println(t.buy("Beer", purse));  // false, not enough money :((
    System.out.println(t.buy("Small beer", purse));  // true, better small than nothing..
