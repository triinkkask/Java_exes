
Realiseerida depoo infosüsteem, millega saab luua ronge kaupade vedamiseks.

Kaust Gitis: ``EX12Depot``

Pakk: ``ee.ttu.iti0202.depot``

Kaup (*cargo*)
==============

Rongidega veetakse erinevaid kaupu. Igal kaubal on riskitase, ehk siis mõni kaup on ohtlikum kui teine.

Lisaks on erinevatel kaupadel seosed, mis ei luba neid koos vedada.

Realiseerida 5 erinevat tüüpi kaupa: inimesed, puit, kütus, tuli, vesi.

Inimesed ei saa koos sõita järgmiste kaupadega: kütus, tuli.
Puit ei saa koos sõita tulega, veega.
Kütus ei saa koos tulega sõita.
Tuli ei saa koos veega sõita.
Vesi ei saa koos kütusega sõita.

Riskitasemed: ``inimene < puit < vesi << kütus < tuli``

Rong (*train*)
==============

Rong koosneb ühest vedurist ja vagunitest. Rongil võib olla ka 0 vagunit.
Rongil on ülevaade vedurist ja vagunitest ning vagunite summaarsest riskitasemest.


Vedur (*engine*)
================

Rongil peab olema vedur. Veduril on lubatud riskitase. Ehk siis see vedur saab vedada vaid niipalju kaupa, kuni ta riskitase lubab.

Vagun (*car*)
=============

Vagun võib sisaldada kaupa või olla tühi. Ühes vagunis saab olla vaid ühte tüüpi kaup. Depoos olles on vagun tühi.

Vagunil on ülevaade temas sisalduva kauba kohta (või on see olemata, st vagun on tühi).

Depoo (*depot*)
===============

Depoos on vedurid ja vagunid. Neid saab depoosse lisada. Samuti saab depoost küsida vedurit ja vagunit.
Mõlemal juhul peab arvestama, et depoos ei pruugi vastavat seadet olla, mille peale meetodid peavad tagastama vastavalt mitte midagi.

Depool on meetod rongi koostamiseks. Selleks saab meetodile ette anda kaubad, mida rong peab vedama. Argumente võib olla 0 või rohkem (*varargs*).
See meetod tagastab uue rongi, kui see õnnestub. Rongi jaoks tuleb võtta depoost vedur. Kui vedurit pole, siis meetod ei tagasta rongi.
Argumentidena ette antud kaubad tuleb võtta järjest ja lisada vagunitesse.
Kui depool rohkem vaguneid pole, siis osa kaupa jääb vedamata (rong tuleb lühem kui plaanitud). Ehk kui on vaja vedada: vesi, vesi, inimesed; ning kui depoos on 2 vagunit, jäävad inimesed välja.
Kaupade lisamisel tuleb jälgida, et riskitase ei läheks veduri jaoks liiga suureks.
Kui mõne kauba lisamisel ületaks kogu riskitase veduri lubatud taseme, siis jääb see kaup vahele. Aga järgmisi kaupu proovitakse sellegipoolest lisada.

Kui rongi koostamisel proovitakse lisada mittesobivaid kaupu, siis rongi ei tagastata.

Depool on ülevaade kõikidest koostatud rongidest, depoos olevatest veduritest ja vagunitest.

Main
====

Kirjuta ``main`` meetod, milles tehakse läbi vähemalt järgmised olukorrad:

- lisatakse depoosse vedureid ja vaguneid
- proovitakse teha rongi nii, et vedurit pole
- proovitakse teha rongi nii, et vaguneid pole
- proovitakse teha rongi, kus riskitase ületab veduri lubatud taseme
- proovitakse teha rongi, kus osa kaupu ei sobi omavahel.

Boonus
======

Boonusosas lisad erindid, veduri konfliktsed kaubad, veduri valimise ja testid.

Boonusosa jaoks realiseerida erindid:

- kui üritatakse mittesobivad kaubad kokku panna
- kui vedurit depoos ei ole
- kui rongile üritatakse lisada liiga riskantset vagunit (millega ületatakse veduri riskitase).
  Seda erindit tuleb kasutada rongi loomisel depoos. Ehk siis selle erindi puhul jääb see kaup/vagun lisamata.

Veduril on samamoodi konfliktsed kaubad. Näiteks, veduriga ei tohi vedada kütust. Rongi loomisel tuleks valida vedur, millega saab kõige rohkem kaupa ära vedada (kogu rongi riskitase oleks suurim).

``main`` meetodis peavad olema näited, kuidas erindeid püütakse ning kuidas otsitakse sobivaim vedur.

Kirjuta testid.