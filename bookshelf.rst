Bookshelf
==========

``Kaust Gitis: EX03Bookshelf``

``Pakk Gitis: ee.ttu.iti0202.bookshelf``

Raamatuvahetus
--------------

Lood infosüsteemi raamatuvahetuse jaoks. Inimesed saavad raamatuid vahetada. Raamatul on hind. Kui raamat vahetab omanikku,
saab vana omanik raamatu hinna võrra raha tagasi, uus omanik aga peab selle sisse maksma.

*Märkus: Tegelikult oleks õigem lahendus selline, kus meil on eraldi objekt "raamatupoe" jaoks. See raamatupood hoiaks siis raamatute infot.*
Selles ülesandes kasutame staatilisi meetodeid ja muutujaid (peamiselt just seepärast, et hoida süsteem lihtsamana ning näidata staatiliste meetodite kasutamist).

class Book
----------

Realiseerida tuleb järgnevad meetodid:

**Meetodid:**

- ``public Book(String title, String author, int yearOfPublishing, int price)`` Konstruktor, mis võtab sisendiks raamatu pealkirja, autori, väljumisaasta ja hinna.
- ``public String getTitle()`` Tagastab raamatu pealkirja.
- ``public String getAuthor()`` Tagastab raamatu autori.
- ``public int getYearOfPublishing()`` Tagastab raamatu väljumisaasta.
- ``public int getPrice()`` Tagastab raamatu hinna.
- ``public int getId()`` Tagastab raamatu unikaalse identifikaatori. Kõige esimene raamat, mis programmis luuakse, saab id-ks 0, järgmine 1 jne.
- ``public Person getOwner()`` Tagastab omaniku (``Person`` objekti).
- ``public boolean buy(Person buyer)`` Raamat ostetakse etteantud persooni poolt. Meetod tagastab ``true``, kui raamatuost õnnestus, muul juhul ``false``,
  Raamatut ei saa osta raamatu praegune omanik. Raamatut ei saa osta juhul, kui uuel omanikul pole piisavalt raha. Raamatu vana omanik saab raamatu hinna võrra raha juurde.
  See meetod peab kasutama ``Person`` klassi meetodeid ``sellBook`` ja ``buyBook``.
  Kui ``buyer`` on ``null``, siis raamatu praegune omanik (kui selline on) saab raha tagasi (ta müüb raamatu ära) ning uut omanikku ei ole (omanik on ``null``).
- ``public static int getAndIncrementNextId()`` Staatiline meetod, mis tagastab järgmise raamatu identifikaatori (id) ning seejärel suurendab id väärtust.
  Esimene loodud raamat saab id-ks 0, teine raamat 1, kolmas raamat 2 jne. Kui seda meetodit järjest välja kutsuda, peaks saama tulemused 0, 1, 2 jne.

**Täpstusi**

Siia klassi on mõistlik lisada abimeetod, mida saab kasutada juhul, kui persooni objekti kaudu raamatut ostetakse või müüakse. Vt allpool teise klassi täpsustuste alt.

class Person
------------

- ``public Person(String name, int money)`` Konstruktor, saab sisendiks persooni nime ja tema raha koguse.
- ``public String getName()`` Tagastab persooni nime.
- ``public int getMoney()`` Tagastab persooni raha koguse.
- ``public boolean buyBook(Book book)`` Persoon saab raamatu omanikuks. Kui tehing õnnestub, tagastada ``true``, muul juhul ``false``.
  Osta ei saa ``null`` raamatut. Kui persoonil pole piisavalt raha, ei saa raamatut osta. Kui raamatul on juba omanik, siis raamatut osta ei saa.
  Peale edukat ostmist persooni raha kogus väheneb raamatu hinna võrra.
- ``public boolean sellBook(Book book)`` Persoon müüb raamatu. Kui tehinb õnnestub, tagastada ``true``, muul juhul ``false``.
  Müüa ei saa ``null`` raamatut. Müüa saab vaid raamatut, mille omanik antud persoon on.
  Peale edukat müümist persooni raha kogus suureneb raamatu hinna võrra.

**Võite julgelt luua uusi meetodeid** (ilma on keeruline).

**Täpsustusi**

``buyBook()`` meetod ei tohiks kasutada ``sellBook()`` meetodit ja vastupidi. Ehk siis ühe persooni kaudu ostmine ei tohiks mõjutada teist persooni
(näiteks, et kui A ostab raamatu, siis eelmine omanik B saab juba raha tagasi).

``buyBook()`` ja ``sellBook()`` peavad töötama ka eraldi välja kutsudes. Ehk siis ülesande peamine rõhk on meetodil ``Book.buy()``, aga ostmine / müümine
persooni kaudu peab ka töötama. Ostmise puhul tuleb veenduda, et raamatul juba pole omanikku. Müümisel tuleb veenduda, et müüja omab müüdavat raamatut
(ei saa müüa raamatut, mis ei kuulu talle).

Boonus
=======

Täienda klassi ``Book`` järgnevate meetoditega:

- ``public static Book of(String title, String author, int yearOfPublishing, int price)`` - loob uue raamatu instantsi, kui sellise nime, autori ja aastaga raamatut juba ei ole.
  Ehk siis meetod peab teadma, millised raamatud on juba loodud. Kui sellist raamatut veel pole loodud, siis tagastab uue ``Book`` instantsi, muul juhul juba olemasoleva raamatu instantsi.
  **Tähelepanu** Arvesse tuleb võtta vaid raamatuid, mis on loodud ``of`` meetodiga. Tavalise konstruktoriga raamatud siin arvesse ei lähe (nende osas ei pea arvestust pidama).
- ``public static Book of(String title, int price)`` - sarnane eelmisele meetodile. Selleks, et oleks mugavam raamatuid lisada, saab määrata vaid pealkirja ja hinna.
  Autor ja aastaarv võetakse eelmisena sisestatud raamatu pealt. Näiteks olukorras, kus lisatakse ühe autori raamatuid järjest, ei pea eraldi autorit iga kord sisestama.
  Kui see meetod kutsutakse välja esimesena (eelmist raamatut pole sisestatud), tagastab ``null``.
- ``public static List<Book> getBooksByOwner(Person owner)`` - tagastab kõik etteantud omaniku raamatud.
- ``public static boolean removeBook(Book book)`` - eemaldab raamatu kõikide raamatute nimekirjast.
  Kui see raamat on kellegi oma, siis ta saab saab raamatu hinna võrra rikkamaks (ehk siis müüb raamatu).
  Kui etteantud raamat on ``null`` või sellist raamatut üldse pole, tagastab meetod ``false``, muul juhul ``true``.
- ``public static List<Book> getBooksByAuthor(String author)`` - tagastab kõik raamatud, mille autor vastab täpselt otsitud autorile. Otsing ei ole tõstutundlik (Mati == MATI == mati).
  Kui ühtegi raamatut ei leita, tagastab tühja listi.

Täienda klassi ``Person`` järgnevate meetoditega:

- ``public List<Book> getBooks()`` - tagastab kõik omaniku raamatud. Kui raamatuid pole, siis tagastab tühja listi. Alguses persoonil ühtegi raamtut pole.
  Raamatute kogus suureneb ostes ning väheneb müües.

Lisaosas peab seekord natuke tähelepanu pöörama ka ajalisele efektiivsusele. Teie koodi testitakse väga suurte hulkadega. Seega mõistlik on plaanida meetodid nii,
et need oleks võimalikult efektiivsed. Ehk siis kohtades, kus tingimata pole vaja kõiki raamatuid läbi käia, võiks seda mitte teha.

Põhiosa mall
----------------

.. code-block:: java

    package ee.ttu.iti0202.bookshelf;

    public class Book {
        public static int getAndIncrementNextId() {
            return 0;
        }

        public Book(String title, String author, int yearOfPublishing, int price) {
        }

        public String getTitle() {
            return null;
        }

        public String getAuthor() {
            return null;
        }

        public int getYearOfPublishing() {
            return 0;
        }

        public Person getOwner() {
            return null;
        }

        public int getPrice() {
            return 0;
        }

        public int getId() {
            return -287;
        }

        public boolean buy(Person buyer) {
            return false;
        }

    }


**Person.java**

.. code-block:: java

    package ee.ttu.iti0202.bookshelf;


    public class Person {
        public Person(String name, int money) {
        }

        public int getMoney() {
            return 0;
        }

        public String getName() {
            return null;
        }

        public boolean buyBook(Book book) {
            return false;
        }

        public boolean sellBook(Book book) {
            return false;
        }
    }

Testimiseks **Main.java**:

.. code-block:: java

    package ee.ttu.iti0202.bookshelf;

    public class Main {
        public static void main(String[] args) {
            Book tammsaare = new Book("Truth and Justice", "Tammsaare", 1926, 100);
            Book meri = new Book("Silverwhite", "Meri", 1976, 200);

            Person mati = new Person("Mati", 200);
            Person kati = new Person("Kati", 300);

            System.out.println(mati.buyBook(tammsaare)); // true
            System.out.println(mati.getMoney());  // 100
            System.out.println(tammsaare.getOwner().getName()); // Mati

            System.out.println(mati.sellBook(tammsaare)); // true
            System.out.println(mati.getMoney()); // 200
            System.out.println(tammsaare.getOwner()); // null

            System.out.println(mati.sellBook(tammsaare)); // false

            System.out.println(mati.buyBook(meri)); // true
            System.out.println(mati.getMoney()); // 0

            System.out.println(meri.buy(kati)); // true
            System.out.println(mati.getMoney()); // 200
            System.out.println(kati.getMoney()); // 100
            System.out.println(meri.buy(kati)); // false
            System.out.println(kati.getMoney()); // 100
            System.out.println(meri.getOwner().getName()); // Kati
            System.out.println(kati.sellBook(meri)); // true
            System.out.println(meri.getOwner()); // null

            // id
            System.out.println(tammsaare.getId()); // 0
            System.out.println(meri.getId()); // 1
            System.out.println(Book.getAndIncrementNextId()); // 2

        }
    }

**Boonusosa** testimiseks võib järgneva koodi lisada ``main`` meetodisse:

.. code-block:: java

    // BONUS
    Book b0 = Book.of("Java EX00", 1);
    System.out.println(b0); // null
    Book b1 = Book.of("Java EX01", "Ago Luberg", 2018, 3);
    Book b2 = Book.of("Java EX02",4);
    System.out.println(b2.getAuthor()); // Ago Luberg
    Book b3 = Book.of("Java EX03",7);
    Book b4 = Book.of("Java EX01", 11);
    System.out.println(b1 == b4); // true

    Book harry1 = Book.of("Harry Potter: The Philosopher's Stone", "J. K. rowling", 1997, 1000);
    Book harry2 = Book.of("Harry Potter: The Chamber of Secrets", "J. K. Rowling", 1998, 1000);
    List<Book> rowlingBooks = Book.getBooksByAuthor("j. k. rowling");
    System.out.println(rowlingBooks.size()); // 2
    System.out.println(rowlingBooks.get(0).getTitle()); // Harry Potter: The Philosopher's Stone
    System.out.println(rowlingBooks.get(1).getAuthor()); // J. K. Rowling

    Person bonusPerson = new Person("Joonas Boonus", 10000);
    b1.buy(bonusPerson);
    bonusPerson.buyBook(harry1);

    List<Book> personBooks = Book.getBooksByOwner(bonusPerson);
    System.out.println(personBooks.size()); // 2
    System.out.println(personBooks.contains(b1));  // true
    System.out.println(personBooks.contains(harry1)); // true
    System.out.println(bonusPerson.getMoney()); // 8997

    Book.removeBook(b1);
    personBooks = Book.getBooksByOwner(bonusPerson);
    System.out.println(personBooks.size()); // 1
    System.out.println(personBooks.contains(b1)); // false
    System.out.println(bonusPerson.getMoney()); // 9000