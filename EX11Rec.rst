
Põhiosa
=======

Põhiosa ülesanded lähevad kausta ``EX11Rec``. Selle sees pakki ``ee.ttu.iti0202.rec`` ning klassi nimi on ``Rec``.

Kui mõnda ülesande osa ei realiseeri, peaks vastava meetodi ikka looma. Vastasel korral test ei kompileeru.

Maksimaalse elemendi leidmine listist
-------------------------------------

Kirjuta meetod ``public static int maxElement(List<Integer> nums)``, mis leiab etteantud listist suurima väärtusega elemendi.
Listis on alati vähemalt üks element.

Listi puhul võib "elemendi eemaldamiseks" kasutada ``nums.subList(1, nums.size())``.

::

    maxElement(List.of(1, 2, 3)) => 3
    maxElement(List.of(3, 2, 1)) => 3
    maxElement(List.of(6)) => 6
    maxElement(List.of(-6)) => -6

Maksimaalne element, abimuutujaga
---------------------------------

Kirjuta meetod ``public static int maxElement(List<Integer> nums, int max)``, mis leiab etteantud listist ``nums`` suurima väärtusega elemendi.
Teine parameeter ``max`` on abimuutuja, millesse saab jooksva parima tulemuse salvestada.

Näiteks kui list on järgmine ``1, 4, 6, 3, 7``, siis esimese väljakutse korral on ``max = 0`` (või veel parem, miinus lõpmatus - miks 0 ei sobi?).

Võtame järgmise (esimese) elemendi ``1`` ja võrdleme ``max`` väärtusega. Kuna ``1`` on suurem, "jätame" selle meelde. Meetod kutsutakse välja argumendiga ``max = 1``.

Järgmise sammu puhul on element ``4``. Kuna see on praegusest ``max`` väärtusest suurem, siis kutsutakse järgmine samm välja ``max = 4`` väärtusega.

Kui jõuame elemendini ``3``, siis sel hetkel ``max = 6``. Kuna element on sellest väiksem, siis järgmine sammu kutsutakse ``max = 6`` väärtusega (ehk siis ``max`` ei muutu).

Kui jõutakse listi lõppu, saab tagastada ``max`` väärtuse.

Ehk siis igal sammul tagastatakse see tulemus, mis tuleb järgmisest sammust. ``max`` argumenti kasutame selleks, et sinna salvestada seni parim tulemus. Kui jõuame lõppu, tagastame ``max`` väärtuse.

::

    maxElement(List.of(1, 2, 3), 0) => 3
    maxElement(List.of(3, 2, 1), 0) => 3
    maxElement(List.of(6), 0) => 6
    maxElement(List.of(-6), 0) => 0
    maxElement(List.of(-6), -100) => -6

Pikim järjestikune kasvav alamjada
------------------------------------------

Kirjuta meetod ``public static int maxGrowth(List<Integer> nums)``, mis tagastab kõige pikema järestikuse alamajada pikkuse.
Järjestikune alamjada tähendab järjestikuseid elemente. Kasvav selline alamjada tähendab seda, et alamjadas iga element on väiksem temale järgnevast elemendist.

Näiteks on järjestikused kasvavad alamjadad järgmised: ``1, 4, 5``, ``5, 6``, ``-5, -2, 2, 4``, ``1``.

Aga järgmised **ei ole** kasvavad (alam)jadad: ``1, 1``, ``3, 2, 1``, ``1, 2, 0, 3``

Selle ülesande lahendamiseks on mõistlik teha abimeetod, milles sarnaselt maksimaalse elemendi leidmise meetodile hoitakse abiväärtusi.
Näiteks võib hoida kahte väärtust: praegune kasvavate elementide kogus ja maksimaalne alamjada pikkus.

::

    maxGrowth(List.of(1, 2, 3)) => 3
    maxGrowth(List.of(3, 2, 1)) => 1
    maxGrowth(List.of(1, 2, 3, 1, 2, 3, 4)) => 4
    maxGrowth(List.of(1, 2, 3, 3, 1, 5, 6)) => 3


Mall
----

.. code-block:: java

    package ee.ttu.iti0202.rec;

    import java.util.List;

    public class Rec {
        public static int maxElement(List<Integer> nums) {
            return 0;
        }

        public static int maxElement(List<Integer> nums, int max) {
            return 0;
        }

        public static int maxGrowth(List<Integer> nums) {
            return 0;
        }
    }

Boonus
======

Selle ülesande lahendus läheb pakki ``ee.ttu.iti0202.tree`` ja klassi ``Node``.

Puu
---

Kirjutada lihtne kahendpuu (igal sõlmel on kaks järglast). Puu koosneb sõlmedest (*node*), igal sõlmel on kaks järglast (*children*), nimetame neid *left* ja *right*. Need järglased on omakorda sõlmed jne.
Igal sõlmel on lisaks ka üks väärtus (antud ülesandes täisarv). Iga sõlme puhul peab kehtima reegel: kõik väärtused, mis jäävad vasaku (*left*) järglase (alamsõlmede) külge, on sõlme enda väärtusest väiksemad;
kõik väärtused, mis jäävad parema (*right*) järglase (alamsõlmede) külge, on sõlme enda väärtusest suuremad. Antud ülesandes lisatakse puusse vaid erinevaid elemente.

Tuleb realiseerida klass ``Node`` meetoditega:

- ``public Node()`` - konstruktor ilma argumendita - sõlmele väärtust ei lisata.
- ``public Node(Integer value)`` - konstruktor, millega lisatakse sõlmele väärtus.
- ``public void addValue(Integer value)`` - väärtus lisatakse puusse. Kui sõlmel on juba väärtus olemas, kutsutakse sama meetod välja alamsõlmele.
  Kui näiteks sõlmel on väärtus 5 ja listakse väärtus 4, siis tuleb uus väärtus (4) panna vasakule alampuusse. Ehk siis kutsutakse *left* sõlme peal ``addValue`` välja.
  Seda korraatakse seni, kuni jõutakse sõlmeni, kus väärtust veel ei ole - sel juhul lisatakse väärtus sellesse sõlme.

- ``public List<Integer> getValues(boolean asc)`` - tagastab kõik elemendid **järjestatult**. Kui ``asc`` on tõene, siis kasvavas järjekorras, muul juhul kahanevas järjekorras.
  Kuna oleme elemendid puusse lisanud süsteemselt (iga sõlme puhul selle väärtusest väiksed elemendid on temast vasakul alampuus ja vastupidi, siis saab väga lihtsalt elemendid järjesetada).
  Üldine reegel näiteks kasvavate elementide jaoks on selline: võta kõik vasaku alampuu väärtused (``getValues``), liida juurde praeguse sõlme väärtus ja lisa kõik parema alampuu väärtused.

- ``public Optional<Node> getNode(Integer value)`` - tagastab ``Node`` objekti, millel on küsitud väärtus. Kui sellist sõlme pole, tagastab ``Optional.empty()``.
- ``public int getMaxDepth()`` - tagastab puu maksimaalse sügavuse. Kui puus on üks element, on selle sügavus 0. Kui puus on kaks elementi, on selle sügavus 1.
- ``public Integer getMinValue()`` - tagastab puus oleva minimaalse väärtuse (eeldame, et puus on alati vähemalt üks element).
- ``public Integer getMaxValue()`` - tagastab puus oleva maksimaalse väärtuse (eeldame, et puus on alati vähemalt üks element).
- ``public Integer getValue()`` - tagastab sõlme väärtuse


::

    Node node = new Node(3); // first node with value 3
    node.addValue(2);
    node.addValue(1);
    node.addValue(7);
    node.addValue(5);

    System.out.println(node.getValues(true)); // [1, 2, 3, 5, 7]
    System.out.println(node.getValues(false)); // [7, 5, 3, 2, 1]

    System.out.println(node.getMinValue()); // 1
    System.out.println(node.getMaxValue()); // 7

    System.out.println(node.getNode(3)); // Optional[3]
    System.out.println(node.getNode(7)); // Optional[7]
    System.out.println(node.getNode(8)); // Optional.empty
    System.out.println(node.getMaxDepth()); // 2
    System.out.println(node.getValue()); // 3
    System.out.println(node.getNode(7).get().getValue()); // 7

Eelnevas näites tekib selline puu:

::

        3
       /  \
      2     7
     /     /
    1     5

Esimene väärtus, mis lisatakse, saab ka loodud sõlme väärtuseks. Seejärel lisatakse ``2``, mis läheb sõlmest vasaku järlase väärtuseks. Väärtus ``1`` läheb sõlme ``2`` vasaku alamsõlme väärtuseks.
Kuna ``7`` on sõlme väärtusest (``3``) suurem, läheb see parema alamsõlme väärtuseks. ``5`` on esialgu sõlme väärtusest (``3``) suurem, seega liigub edasi paremale alamsõlmele.
Seal on aga sõlme väärtus ``7``, millest ``5`` läheb vasakule alamsõlme väärtuseks.

Puu sügavus:

::

               6
              / \
             3   9
            / \   \
           1   5   11
                    \
                     14

Selle puu sügavus on 3.

::

    1
     \
      2
       \
        3
         \
          4
           \
            5

Selle puu sügavus on 4.


Koodinäide puu sügavuse kohta:

::

    Node n1 = new Node();
    n1.addValue(1);
    System.out.println(n1.getMaxDepth()); // 0
    n1.addValue(2);
    System.out.println(n1.getMaxDepth()); // 1
    n1.addValue(3);
    System.out.println(n1.getMaxDepth()); // 2
    n1.addValue(4);
    n1.addValue(5);
    n1.addValue(6);
    System.out.println(n1.getMaxDepth()); // 5
    n1.addValue(8);
    n1.addValue(7);
    System.out.println(n1.getMaxDepth()); // 7
    n1.addValue(9);
    System.out.println(n1.getMaxDepth()); // 7

Tekib selline puu:

::

    1
     \
      2
       \
        3
         \
          4
           \
            5
             \
              6
               \
                8
               / \
              7   9
