Decoder
=======

``Kaust Gitis: EX02Decoder``

``Pakk Gitis: ee.ttu.iti0202.decoder``

Kirjutada programm, mis dešifreeriks kindla tähestiku
alusel sõnumeid.

class Decoder
-------------

**Meetodid:**

- ``decodeMessage(String message)`` - Meetod dešifreerib Stringina kaasaantud sõnumi. Sõnum koosneb numbritest, mis viitavad tähe asukohale sõnastikus. 
  Näiteks sõnastikuga "abcd" ning sõnumiga "3021" oleks dešifreeritud sõnum "dacb". 
  Juhul, kui tähe asukoht on kahekohaline, on ta sõnumis kujul x+y (1+3 => 13). 
  Põhiosas on sõnasiku pikkus maksimaalselt 100, mis tähendab, et viimane indeks on 99 (ehk kahekohaline).
  Kui plaanid lisaosa teha, tasub mõelda üldisema lahenduse peale (vt. altpoolt).
  Juhul, kui sõnumis esineb sümbol $, on järgmine täht Suur. 
  Lahendus võib eeldada, et iga antud indeks on sõnastiku piirides.
  Kõik sümbolid peale numbrite, ``+`` ja ``$`` esinevad tulemuses täpselt samamoodi. Sõnastikuga "abc" ja sõnumiga "a2b1c0!" oleks tulemus "acbbca!".

**Boonus:**

- Sõnumis võib indeksi väärtus olla rohkem kui kahekohaline. Sel juhul on arv sõnumis kujul x+y+.. (1+2+3 = 123 ; 1+0+0+2+1 = 10021)
- Realiseerige ``main`` meetodis sõnumite järjestikune dešifreerimine skänneri abil konsoolist lugedes. 
  Sisendi lugemine peaks töötama järgmiselt: kõigepealt küsitakse kasutajalt sõnastik, seejärel küsitakse 1 kuni mitu korda sõnumit.
  Iga sisestatud sõnum dekodeeritakse kasutades esimesena sisestatud tähestikku.
  Erinevad sisendid (sõnastik, sõnum1, sõnum2 jne) on eraldatud reavahetusega (``\n``).
  
Näide võimalikust sisend-väljundist:

::

    Enter dictionary: abc
    Enter message: $001
    Decoded message: Aab
    Enter message: 112
    Decoded message: bbc
    
    ^D

``ctrl`` + ``D`` saab kasutada IntelliJ konsoolis, et lõpetada standardsisend. Testides saadetakse teile kindla pikkusega sisend (näiteks 3 rida: ``abc\n$001\n112``),
ise testides saate sisendi lõpetada mainitud klahvikombinatsiooniga.

Eelnev oli vaid näide, võite kasutada muid tekste sisendi küsimisel (neid tester ei kontrolli).



Mall
----

.. code-block:: java


    package ee.ttu.iti0202.decoder;
    
    public class Decoder {
    
        /**
         * Method deciphers given message based on the dictionary.
         * 
         * The message consists of a series of numbers, which point to a letter's
         * index location in the dictionary. e.g. with a dictionary of "abcd" and
         * message of "3021" the deciphered message would be "dacb".
         * 
         * If the index of the letter is comprised of 2 or more numbers, the message will
         * read as those numbers separated by plus signs. e.g. 1+3 => 13 ; 1+2+3 => 123.
         * 
         * For every "$" symbol that appears in the message, the next
         * letter must be converted to upper case.
         * 
         * Any other symbols appearing in the message should be added to the
         * deciphered message untouched.
         * 
         * @param dictionary - dictionary to be used.
         * @param message - message to be deciphered.
         * @return - deciphered message.
         */
        private static String decodeMessage(String dictionary, String message) {
            return "";
        }
        
        public static void main(String[] args) {
            System.out.println(Decoder.decodeMessage("a", "")); // =>

            System.out.println(Decoder.decodeMessage("", "Hello world!")); // => Hello world!

            System.out.println(Decoder.decodeMessage("abcdefghijklmnopqrstuvwxyz", "$741+11+11+4 2+21+41+71+13!")); // => Hello world!
        
            System.out.println(Decoder.decodeMessage("onetw", "$012, 340")); // => One, two
    
            System.out.println(Decoder.decodeMessage("catnip", "$0$1$5$2$1$4$3")); // => CAPTAIN
    
            System.out.println(Decoder.decodeMessage("race", "0123210")); // => racecar
    
            // Bonus
    
            // Dictionary of 100 * "a" + "abcdefghijklmnopqrstuvwxyz"
            StringBuilder dictionary  = new StringBuilder();
            for (int i = 0; i < 100; i++) dictionary.append("a");
            dictionary.append("abcdefghijklmnopqrstuvwxyz");
    
            System.out.println(Decoder.decodeMessage(dictionary.toString(), "$1+0+71+0+41+1+11+1+11+1+4 1+2+21+1+41+1+71+1+11+0+3!")); // => Hello world!

        }
    
    }

Vihjeid põhiosa kohta
---------------------

Põhiosas võib eeldada, et maksimaalselt on kahekohalised indeksid. Võib mõelda, et meil on 4 erinevad olukorda:

- ``X``, kus X on number, mis tähistab sõnastiku indeksit
- ``$X``, kus sõnastikust leitud sümbol tuleb suureks muuta
- ``X+Y`` - kahekohaline indeks
- ``$X+Y`` - kahekohaline indeks, tuleb suureks muuda

Kõige laisem lahendus oleks selline, kus iga olukord on eraldi if-lausega lahendatud. Seda aga me ei soovi lahenduses näha (saab stiili eest vähem punkte näiteks).
Pigem võiks proovida mõelda mingi järgmise lahenduse peale:

::

    Loome tsküli, kus i = [0 .. message.length()):
        kui message[i] on $:
            siis jätame meelde, et täht on vaja suureks muuta
            suurendame i-d
        kui message[i] on number:
            paneme selle int muutujasse nr
            kui message[i + 1] on + ja message[i + 2] on number:
                lisame nr-ile vastavalt juurde, et tekiks kahekohaline arv
                suurename i-d kahe võrra
            kui on vaja suureks teha:
                lisame uude sõnumisse suureks tehtud sõnastiku tähe positsioonil nr
            muul juhul:
                lisame uude sõnumisse sõnastiku tähe positsioonil nr
        muul juhul (kui message[i] ei ole number):
            lisame uude sõnumisse tähe message[i]
            
Sellise koodiga peaks saama lahendatud. Kindlasti on erinevaid viise, kuidas seda lahendada.