Mystic orbs
===========

``Kaust Gitis: EX05MysticOrbs``

``Pakk Gitis: ee.ttu.iti0202``

class ResourceStorage
---------------------

``Pakk Gitis: ee.ttu.iti0202.storage``

Koht, kust ahjud võtavad ressursse.

Meetodid:

NB! Kõik meetodid, mis võtavad ressursi nime sõnena, on **case-insensetive** (ehk siis suur- ja väiketäht loetakse samaks: ``pearl``, ``Pearl``, ``PEARL`` jne on kõik võrdsed).

**Ressurssi võiks vaadelda tavalise sõnena.**

- Vaikekonstruktor, mis ei tee midagi erilist. *(Vihje: võiks selle kirjutamata jätta. Kui ükski konstruktor ei ole defineeritud, siis Java kompilaator genereerib vaikekonstruktori automaatselt.)*

- ``public boolean isEmpty()`` - meetod kontrollib, kas on veel ressursse. Tagastab ``false``, kui leidub vähemalt üks ressurss, mille kogus on rohkem kui 0. Vastasel juhul tagastab ``true``.

- ``public void addResource(String resource, int amount)`` - lisab *amount* tükki ressurssi hoidlasse. Kui selline ressurss on juba hoidlas olemas, siis selle kogus suureneb. Kui ressursi nimi on tühi, siis seda ei lisata.

Vihje: Javas on olemas andmestruktuur, mis laseb hoida andmeid viisil "key-value". Siin ``key`` võiks olla ressursi nimi ja ``value`` - ressursi kogus.

- ``public int getResourceAmount(String resource)`` - tagastab ressursi koguse. Kui sellist ressurssi pole või see on otsa saanud, tagastab 0.

- ``public boolean hasEnoughResource(String resource, int amount)`` - meetod kontrollib, kas leidub niipalju ressursse hoidlast. Kui ``amount`` on väiksem 1-st, tagastab false.

- ``public boolean takeResource(String resource, int amount)`` - meetod ressursi väljavõtmiseks. Kui mingil põhjusel ei saa niipalju ressursse välja võtta, tagastab ``false``. Kui aga õnnestub, siis tagastab ``true``.

class Orb
---------

``Pakk Gitis: ee.ttu.iti0202.orb``

Maagiline kuul oma energiaallikaga.

Meetodid:

- ``public Orb(String creator)`` - konstruktor, mis määrab kuulile ahju nime, milles see toodeti.

- ``public void charge(String resource, int amount)`` - kuuli laadimine. Kuuli energia suureneb väärtuse võrra, mis võrdub ressursi nime sümbolite arvuga korrutatud ``amount``'iga.
  Kui ressursiks on tolm (``dust``) või ressursi nimi koosneb ainult tühikutest, siis ei juhtu midagi.

- ``public int getEnergy()`` - tagastab energia praeguse väärtuse.

- ``public String toString()`` - tagastab rea kujul ``Orb by [creator]``, kus *[creator]* asemel on selle ahju nimi, mis on selle kuuli tootnud. Nt. ``Orb by Ahi1``.

class Oven
----------

``Pakk Gitis: ee.ttu.iti0202.oven``

Ahi, mis oskab luua kuule kindlatest ressurssidest.

Meetodid:

- ``public Oven(String name, ResourceStorage resourceStorage)`` - konstruktor, mis loob ahju instantsi. Võtab ette ahju nime ja viite ressursside hoidlale.

- ``public String getName()`` - tagastab ahju nime.

- ``public ResourceStorage getResourceStorage()`` - tagastab viite ressursside hoidlale, kust see ahi võtab ressursse.

- ``public int getCreatedOrbsAmount()`` - tagastab arvu, mis on võrdne juba loodud kuulide arvuga.

Kui ahi loob liiga palju kuule, siis see läheb katki. Tavalise ahju puhul "läks katki" tähendab seda, et ta on loonud 15 kuuli.

- ``public boolean isBroken()`` - tagastab ``true``, kui ahi on katki, või ``false``, kui see on korras.

- ``public Optional<Orb> craftOrb()`` - loob kuuli, kui ahi ei ole katki ja kui on piisavalt ressursse. Kui mingil põhjusel ei saa kuuli luua, tagastab tühja ``Optional``'i.

**Optional:** http://javarevisited.blogspot.com.ee/2017/04/10-examples-of-optional-in-java-8.html ja https://ained.ttu.ee/javadoc/oop-optional.html

Kuuli loomiseks on vaja järnevaid ressursse: ``pearl`` x1, ``silver`` x1.

Neid ressursse küsib ahi ressursside hoidlast (``ResourceStorage``). Peale loomist peavad need ressursid selles koguses hoidlast ära kaduma.

**Kuuli loomine tähendab seda, et luuakse kuul, määratakse talle looja nimi (ahju nimi) ja laetakse kasutatud ressurssidega.**

class MagicOrb
--------------

**Laiendab klassi Orb, ehk on klassi Orb alamklass.**

``Pakk Gitis: ee.ttu.iti0202.orb``

Kuul, mis suudab neelata kaks korda rohkem energiat, kui tavakuul. See on maagia.

Meetodid:

- Sama konstruktor, mis klassil ``Orb``.

- **Ülekirjutatud meetod** ``public void charge(String resource, int amount)`` - sama, mis ülemklassis, aga kuul saab 2 korda rohkem energiat.

- ``public String toString()`` - tagastab rea kujul ``MagicOrb by [creator]``, kus *[creator]* asemel on selle ahju nimi, mis on selle kuuli tootnud. Nt. ``MagicOrb by Ahi1``.

class SpaceOrb
--------------

**Laiendab klassi Orb, ehk on klassi Orb alamklass.**

``Pakk Gitis: ee.ttu.iti0202.orb``

Kuul, mida ei saa laadida, aga tal on oskus neelata nõrgemaid kuule.

- Sama konstruktor, mis klassil ``Orb``. Määrab energiale väärtuse ``100``.

Nõue: Energia väärtuse hoidmiseks ei tohi uut globaalset muutujat teha, vaid on vaja muuta ülemklassi muutuja väärtus.
Kuna sellel on nähtavus ``private``, siis seda alamklassis muuta ei saa. Võib olla võiks panna sellele kõrgem nähtavus? *Et näiteks kõik klassid selles paketis saaks seda muutujat näha/muuta.*

- **Ülekirjutatud meetod** ``public void charge(String resource, int amount)`` - ei tee midagi. Seda tüüpi kuuli ei saa laadida.

- ``public String toString()`` - tagastab rea kujul ``SpaceOrb by [creator]``, kus *[creator]* asemel on selle ahju nimi, mis on selle kuuli tootnud. Nt. ``SpaceOrb by Ahi1``.

- ``public boolean absorb(Orb orb)`` - neelab kuuli. Kuuli saab neelata ainult siis, kui sellel on vähem energiat kui neelajal.
  Neelatud kuulil muutub energia väärtus 0-ks. Neelaja energia väärtus peab suurenema neelatud kuuli energia väärtuse võrra. Tagastab ``true``, kui kuul on neelatud, või ``false``, kui see kuul on tugevam.

class MagicOven
---------------

**Laiendab klassi Oven, ehk on klassi Oven alamklass.**

``Pakk Gitis: ee.ttu.iti0202.oven``

**Iga teine** loodud kuul on maagiline kuul (``MagicOrb``). Läheb katki pärast viiendat loodud kuuli.

- ``public MagicOven(String name, ResourceStorage resourceStorage)`` - konstruktor.

Tuleb üle kirjutada meetodid ``isBroken`` ja ``craftOrb``.

Nii tavalise kui ka maagilise kuuli loomiseks on vaja järnevaid ressursse: ``gold`` x1 ja ``dust`` x3

class InfinityMagicOven
-----------------------

**Laiendab klassi MagicOven, ehk on klassi MagicOven alamklass.**

``Pakk Gitis: ee.ttu.iti0202.oven``

Sama, mis maagiline ahi, aga ei lähe kunagi katki.

- ``public InfinityMagicOven(String name, ResourceStorage resourceStorage)`` - konstruktor.

Kuuli loomiseks on vaja järnevaid ressursse: ``gold`` x1 ja ``dust`` x3

Tuleb üle kirjutada meetod ``isBroken``. *Mida see peab tagastama, kui ahi ei ole kunagi katki?*

class SpaceOven
---------------

**Laiendab klassi Oven, ehk on klassi Oven alamklass.**

``Pakk Gitis: ee.ttu.iti0202.oven``

Unikaalne ahi, mis toodab kosmosekuule. Kui on katki või ei ole piisavalt ressursse kosmosekuuli tootmiseks, siis toodab tavalisi kuule.
Kui aga tavalise kuuli jaoks pole piisavalt ressursse, siis ei saa ahi midagi toota. Läheb katki pärast kahekümne viiendat loodud kuuli.

- ``public SpaceOven(String name, ResourceStorage resourceStorage)`` - konstruktor.

Tavalise kuuli loomiseks on vaja järgnevaid ressursse: ``pearl`` x1, ``silver`` x1.

Kosmosekuuli loomiseks on vaja järgnevaid ressursse: ``meteorite stone`` x1, ``star fragment`` x15.

Tuleb üle kirjutada meetod ``craftOrb``. *Vihje: Kuna kosmosekuule (*``SpaceOrb``*) ei saa laadida, siis seda ei pea tegema.*

class OrbFactory
----------------

Ühise ressursside hoidlaga vabrik, kus mitu ahju toodavad kuule.

``Pakk Gitis: ee.ttu.iti0202.factory``

Meetodid:

- ``public OrbFactory(ResourceStorage resourceStorage)`` - klassi konstruktor, mis saab ette viite ressursside hoidlale.

- ``public void addOven(Oven oven)`` - lisab ahju vabriku ahjude listi. Lisada võib vaid neid ahje, millel on sama ressursside hoidla nagu vabrikul ja mis ei ole juba ahjude listis.

- ``public List<Oven> getOvens()`` - tagastab listi, kus on kõik ahjud, mis töötavad selles vabrikus. Listis peavad ajud olema samas järjekorras, milles need olid sinna lisatud.

- ``public List<Orb> getAndClearProducedOrbsList()`` - tagastab ja tühjendab listi, kus on kõik toodud kuulid. Kui uusi kuule ei ole veel toodud, tagastab tühja listi.

- ``public int produceOrbs()`` - paneb üks kord kõik ahjud käima. Ahjud peavad töötama samas järjekorras nagu need olid lisatud.
  Kõik toodud kuulid tuleb panna listi, mille hiljem saab meetodiga ``getAndClearProducedOrbsList()`` kätte. Listis peavad olema kuulid samas järjekorras, milles töötasid ahjud. Tagastab toodetud kuulide arvu.

- ``public int produceOrbs(int cycles)`` - sama, mis eelmine, aga teeb seda *cycles* korda.

Boonus
======

Osutub, et vigastuse korral mõndasid ahje võib parandada, aga selleks on vaja kulutada ressursse.

interface Fixable
-----------------

``Pakk Gitis: ee.ttu.iti0202.oven``

**Interface:** https://www.tutorialspoint.com/java/java_interfaces.htm

Meetodid:

- ``void fix()`` - parandab ahju. Kui ahi ei ole katki, ei ole vajalikke ressursse või ahju on parandatud liiga palju kordi, siis tõstatab ``CannotFixException`` erindi. Sellest räägime ka hiljem.

- ``int getTimesFixed()`` - tagastab arvu, mitu korda on ahi juba parandatud.

Mall:

.. code-block:: java

    public interface Fixable {
        void fix() throws CannotFixException;

        int getTimesFixed();
    }

Seda liidest peavad implementeerima järgmised klassid: ``MagicOven`` ja ``SpaceOven``.

Realiseerige uued meetodid nendes klassides:

``MagicOven`` - parandamiseks on vaja järgnevaid ressursse: ``clay`` x25 ja ``freezing powder`` x100. Iga järgmine kord suureneb ressursside arv oma algväärtuse võrra.
Ehk kui esimene kord kasutasite 25 tükki ühte ressurssi, siis teine kord peate kasutama 50 tükki, kolmas kord 75 tükki jne. Teise ressurssiga on sama lugu. Seda ahju saab parandada maksimaalselt 10 korda.

``SpaceOven`` - parandamiseks on vaja järgnevat ressurssi: ``liquid silver`` x40. Kui seda ressurssi pole, võib selle asendada ressursiga: ``star essence`` x10.
Kui aga sellist ressurssi pole, siis ei saa ahju parandada. Kui mõlemad ressursid on olemas vajalikus koguses, siis tuleb esimesena kasutada vedalt hõbedat (``liquid silver``).
Kui ahi on parandatud 5 korda, siis seda ei ole enam vaja kunagi parandada.

exception CannotFixException
----------------------------

``Pakk Gitis: ee.ttu.iti0202.exceptions``

**Erind:** https://ained.ttu.ee/javadoc/Exceptions.html

**Erindi loomine ja tõstatamine:** https://ained.ttu.ee/javadoc/oopUserExceptions.html

Meetodid:

- ``public CannotFixException(Oven oven, Reason reason)`` - loob erindi instantsi. Saab ette katkise ahju ja põhjuse, miks seda ei saa parandada.

**Selle klassi sees tuleb defineerida loend (enum)** ``Reason`` **, millel on kolm konstanti (väärtust):** ``IS_NOT_BROKEN``, ``FIXED_MAXIMUM_TIMES``, ``NOT_ENOUGH_RESOURCES``.

- ``public Oven getOven()`` - tagastab ahju, mida ei saa parandada.

- ``public Reason getReason()`` - tagastab põhjuse, miks seda ahju ei saa parandada.


Comparable
----------

Muuta klass ``Oven`` võrreldavaks. Selleks peab see klass implementeerima liidest ``Comparable<Oven>``. (*<Oven>* tähendab seda, et me võrdleme ühte ahju teise ahjuga.)
Seda liidest ei pea iseseisvalt looma, kuna see on Java API liides.

Nüüd peate implementeerima ``Oven`` klassis meetodi ``public int compareTo(Oven o)``. Tuleb arvestada, et argumediks ``o`` võib olla ahju alamtüüp - ehk me saame nt võrrelda tavalist ja kosmoseahju omavahel.

Kasutamise näide:

.. code-block:: java

    Oven o1 = new Oven(...);
    Oven o2 = new SpaceOven(...);

    System.out.println(o1.compareTo(o2)); // comparing two ovens

Meetod ``compareTo`` peab tagastama *int* väärtuse ja see peab olema üks kolmest väärtusest: 1 (o1 > o2), -1 (o1 < o2) või 0 (o1 == o2).

Võrdlemise reeglid:

1. Katkine ahi on alati väiksema prioriteediga kui mitte katkine.

2. Kui mõlemad ahjud on katkised või mõlemad ei ole katkised, siis suurim prioriteet on kosmoseahjul (``SpaceOven``), väiksem prioriteet on maagilisel ahjul (``MagicOven``) ja väikseim prioriteet on tavalisel ahjul (``Oven``).

3. Kui mõlemad ahjud on maagilised, siis suurem prioriteet on sellel, kelle järgmine kuul on maagiline kuul.

4. Kui mõlemad ahjud on maagilised ja nende toodetud kuulide arv on sama, aga üks nendest on ``InfinityMagicOven``, siis sellel on suurem prioriteet.

5. Suurem prioriteet on ahjul, mis on tootnud vähem kuule.

6. Kui nende reeglite järgi on kaks ahju võrdsed, siis tuleb võtta see, mille nimi on suurem. *(Vihje: kahte sõnet võib omavahel võrrelda: str.compareTo(str2))*

7. Kui nimed on ka võrdsed, siis ahjud on võrdsed.

OrbFactory
----------

Uued meetodid:

- ``public List<Oven> getOvensThatCannotBeFixed()``, tagastab listi, mille sees on ahjud, mida ei saa **kunagi** parandada.

- ``public void getRidOfOvensThatCannotBeFixed()`` - eemaldab vabkriku ahjude listist kõik ahjud, mida ei saa kunagi parandada.

Muuta meetod ``produceOrbs`` nii, et kui ahi on katki, siis proovitakse seda parandada.
Kui ahi on parandatud maksimum arv korda, siis tuleb see ahi lisada listi, mida hiljem saab kätte meetodiga ``getOvensThatCannotBeFixed()``.
**Kui ahju parandamise jaoks pole piisavalt ressursse, siis seda ei tohi selles listis olla, kuna see ahi on ikka parandatav.**

- ``public void optimizeOvensOrder()`` - sorteerib ahjude listi. Peale sorteerimist peavad ahjud töötama uues järjekorras. *(Vihje: sorteerimise algoritmi ei pea implementeerima. Võib kasutada seda, et ahjud on omavahel võrreldavad.)*


Näited
------

Seekord tulevad näited JUnit testidena. Me anname teile osa teste ja te saate neid oma arvutis käivitada.
Kuna hiljem tuleb ülesandeid, kus te peate ise teste kirjutama, võivad need testid olla teile näiteks.

Kindlasti ei ole antud testides kõik olukorrad kaetud ja võib juhtuda, et teie lahendus läbib kõik antud testid, aga ebaõnnestub nendes testides, mis meil serveris on.
See tähendab, et teie lahenduses on ikkagi mõni viga ja kui tahate kõrgemat tulemust saada, siis peate selle vea üles leidma ja ära parandama.

.. _Põhiosa_testid: https://ained.ttu.ee/mod/resource/view.php?id=6089

`Põhiosa testid`_

.. _Põhiosa testid: https://ained.ttu.ee/mod/resource/view.php?id=6089

`Boonusosa testid`_

.. _Boonusosa testid: https://ained.ttu.ee/mod/resource/view.php?id=6090

**Neid teste ei ole vaja lahendusega gitti panna.**

Arhiivis on olemas kaust ``EX05MysticOrbs``. Tehke see lahti, võtke sealt kaust ``test`` ja pange oma reposse kausta ``EX05MysticOrbs``. Ehk kaust ``test`` peab olema kaustas ``EX05MysticOrbs``.

Tehke IntelliJ lahti. Moodulis peab juba olema kaust ``test``. Klikkige selle peale parema klahviga, leidke menüüst ``Mark directory as`` ja valige sealt ``Test Source root``. Nüüd kaust ``test`` peab olema märgitud rohelise värviga.

.. image:: https://i.imgur.com/SUKZo61.jpg?1

Võtke suvaline testifail sellest kaustast lahti. Praegu IntelliJ näitab, et ta ei tunne ära ``junit`` teeki ja see on märgitud punase värviga.
Klikkige ``junit`` peale, vajutage klaviatuuril Alt+Enter ja valige sealt ``Add JUnit4' to classpath``.

.. image:: https://i.imgur.com/wZJJNIX.jpg

Nüüd on testid valmis käivitamiseks. Tehke parem klikk ``test`` kausta peale, ja valige ``Run 'All tests'``.
Kui testid ei kompileeru, siis teie lahenduses puuduvad mõned vajalikud klassid/meetodid või nendes on vigu.

**Automaattestimine Javas:** https://ained.ttu.ee/javadoc/unit_testing.html