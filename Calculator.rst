Calculator
==========

``Kaust Gitis: EX01Calculator``

``Pakk Gitis: ee.ttu.iti0202.calculator``

Kirjutada programm, mis kuvab summa või vahe avaldise nö kalkulaatori ekraanil.
Ülesanne on paljudele tuttav programmeerimise algkursusest ja seega tekib hea võrdlusmoment pythoni ja java vahel.

class Calculator
----------------

Realiseerida tuleb järgnevad **staatilised** meetodid:

**Meetodid:**

- ``convertName(String s)`` Tagastab sõne, mis on lühendatud versioon kalkulaatori tootja nimest järgneva loogika järgi: [sõne esimesed kolm tähte suurte tähtedena]-[sõne pikkus][sõne viimased kaks tähte väikeste tähtedena] Kui sõne on lühem, kui kolm tähte, siis tuleb tagastada "ERROR".

- ``addition(int a, int b)`` Tagastab sõne kahe arvu liitvmisel saadud avaldisest. Näiteks "2 + 3 = 5".

- ``subtraction(int a, int b)`` Tagastab sõne kahe arvu lahutamisel saadud avaldisest. Näiteks "5 - 2 = 3"

- ``repeat(String s, int n)`` Tagastab esialgse sõne n kordusega.

- ``line(int width)``, ``line(int width, boolean decorated)`` Tagastab sõne, mis koosneb '-' sümbolitest, kus width määrab sõne pikkuse. Kasuta siin repeat(s, n) funktsiooni. Lisaks peab saama anda teiseks argumendiks boolean väärtuse true või false. Kui teine argument on true, siis sõne algab sümboliga ">" ja lõppeb sümboliga "<", kui see on False, siis koosneb kogu sõne ainult '-' sümbolitest. Kui teist argumenti kaasa ei anta, siis peaks funktsioon käituma samamoodi, kui teine argument oleks false. Kui teine argument on True ja width on 1, tagastada tühi sõne. Siin tuleb kasutada meetodi ülelaadimist, et meetodit saaks kutsuda välja nii ühe kui ka kahe argumendiga.

**Boonus:**

- ``center(String s, int width, LongerPad pad)`` Joondab sõne keskele. Enum väärtus LongerPad.LEFT või LongerPad.RIGHT määrab vastavalt ära kumb pool sõnest peab olema pikemalt tühikutega täidetud juhul, kui sõne ei mahu keskele täpselt ära. Kui määratud laius on sõne pikkusest väiksem, siis tuleb võtta vastava suurusega keskmine osa sellest sõnest. Vaata täpsemalt mallis olevast javadocist.
- ``display(int a, int b, String name, String operation, int width)`` Kuvab summa või vahe avaldise kalkulaatori ekraanil. Siin tuleb kasutada kõiki eelnevalt loodud meetodeid, et kokku aheldada sobiv sõne.


Mall
----

.. code-block:: java

    package ee.ttu.iti0202.calculator;

    public class Calculator {

        private enum LongerPad { LEFT, RIGHT }

        /**
         * Return a string following a naming convention.
         * [first three letters in uppercase]-[length of string][last two letters of string in lowercase]
         * If length of string is less than 3, return "ERROR".
         *
         * @param s original name
         */
        private static String convertName(String s) {
            return "";
        }

        /**
         * Return an expression that sums the numbers a and b.
         * Example: a = 3, b = 7 -> "3 + 7 = 10"
         */
        private static String addition(int a, int b) {
            return "";
        }

        /**
         * Return an expression that subtracts b from a.
         * Example: a = 3, b = 1 -> "3 - 1 = 2"
         */
        private static String subtraction(int a, int b) {
            return "";
        }

        /**
         * Repeat the input string n times.
         */
        private static String repeat(String s, int n) {
            return "";
        }

        /**
         * Create a line separator using "-". Width includes the decorators if it has any.
         *
         * @param width     width of the line, which includes the decorator, if it has one
         * @param decorated if True, line starts with ">" and ends with "<", if False, consists of only "-"
         * If decorated and width is 1, return an empty string ("").
         */
        private static String line(int width, boolean decorated) {
            return "";
        }

        /**
         * Create a line separator using "-".
         *
         * @param width width of the line.
         */
        private static String line(int width) {
            return "";
        }

        /**
         * Center justify a string.
         *
         * "a", 3, LongerPad.LEFT -> " a "
         *
         * When the string does not fit exactly:
         * If LongerPad.LEFT make the left padding longer, otherwise the right padding should be longer.
         * "ab", 5, LongerPad.LEFT  -> "  ab "
         * "ab", 5, LongerPad.RIGHT -> " ab  "
         *
         * If the width is smaller than the length of the string, return only the center part of the text.
         * "abcde", 2, LongerPad.LEFT  -> "bc" or "cd" (both are fine)
         *
         * Return an empty string ("") if the width is negative.
         *
         * @param s string to center
         * @param width width of the outcome
         * @param pad left longer if LongerPad.LEFT, pad right longer if LongerPad.RIGHT
         * @return new center justified string
         */
        private static String center(String s, int width, LongerPad pad) {
            return "";
        }

        /**
         * Create a string representing the display.
         * Use LongerPad.LEFT when centering.
         * Width of the display must be set to the assigned width or expression width, whichever is bigger.
         * If the operation is not valid, display "ERROR".
         *
         * @param a         number
         * @param b         number
         * @param name      full name of calculator company
         * @param operation operation ("addition" or "subtraction") applied to numbers a and b
         * @param width     width of the display
         * @return string representing the final format
         */
        private static String display(int a, int b, String name, String operation, int width) {
            return "";
        }

    }


Näited
------

.. code-block:: java

    public static void main(String[] args) {
        System.out.println(convertName("Burroughs")); // "BUR-9hs"
        System.out.println(convertName("abc")); // "ABC-3bc"
        System.out.println(convertName("")); // "ERROR"
        System.out.println(addition(2, 3)); // "2 + 3 = 5
        System.out.println(repeat("a", 3)); // "aaa"
        System.out.println(line(4)); // "----"
        System.out.println(line(4, true)); // ">--<"

        // bonus
        System.out.println(center("ab", 5, LongerPad.LEFT));  // "  ab "
        System.out.println(center("ab", 5, LongerPad.RIGHT)); // " ab  "
        System.out.println(center("bacon", 3, LongerPad.LEFT));  // "aco"
        System.out.println(center("abcde", 2, LongerPad.LEFT));
        System.out.println(display(2, 3, "abc", "addition", 20));
        /*
         *              ABC-3bc
         * >------------------<
         * |     2 + 3 = 5    |
         * --------------------
         */
        System.out.println(display(1, 1, "cheese", "z", 1));
        /*
         * CHE-6se
         * >-----<
         * |ERROR|
         * -------
         */
    }
