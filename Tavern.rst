Tavern
======

``Kaust Gitis: EX04Tavern``

``Fail Gitis: ee.ttu.iti0202.tavern``

@Hearthstone city, 1337 AC

Kord käis kõrtsmik turul köögi- ja puuvilju ostmas, kuna kõrtsi varud olid lõppemas. Olles äsja puuvilju täis koti kärusse pannud, astus kõrtsmiku juurde tundmatu mees ning lausus: "Vabandage, jälgisin teid huvi pärast ja märkasin, et ostate kokku palju toidukraami". Kõrtsmik noogutas ning vastas, et ta tõesti ostab toitu kõrtsi jaoks.

Seepeale võttis tundmatu mees kusagilt välja kandilise plaadi ja ulatas selle kõrtsmikule öeldes: "Sel juhul pakun välja 10 kuldmündi eest Brimhaveni võlurite parima leiutise, mis oskab teie eest arvutada, palju te kõrtsis raha teenite ning boonuseks oskab see leiutis öelda palju teie külalistel rahakottides münte on".

Lollusena tunduva jutu peale tahtis kõrtsmik midagi teravamat öelda, kuid seepeale hakkas äkki kandiline vidin vilkuma ja sellele ilmus miski, milles väga kauged järeltulijad tunnevad ära Java koodi. Kõrtsmik muutis koheselt meelt ning pooletunnise jutu tulemusena käis kõrtsmik välja 10 kuldmünti. Tundmatu mees seletas, et enne tööd peab vidina ära seadistama.

Sinu, roll on nüüd jälgida tundmatu mehe juhendit ja kandiline vidin tööle saada.

Programm peab sisaldama järgnevaid klasse ja meetodeid:

Klass Currency
---------------

Hoiab infot valuuta kohta. Põhiosas on valuutaks: vask, hõbe ja kuld. Selle klassi jaoks on suurem osa koodi ette antud.

Valuutat saab kasutada selliselt: ``Currency.get("silver")`` - tagastab hõbeda. Uut valuuta objekti luua ei saa (konstruktor on privaatne).
Alguses tuleb lisada kõikvõimalikud valuutad: ``Currency.add("copper")``. Valuutaga koos tuleb määrata kurss baasvaluutas: ``Currency.add("gold", 100)`` .

Realiseeri meetod ``public static int getRate(Currency fromCurrency, Currency toCurrency)``. Seal pead kasutama valuuta kurssi (``getRate()``).

**Mall**

.. code-block:: java

    package ee.ttu.iti0202.tavern;

    import java.util.*;

    /**
     * Keeps track of currencies.
     */
    public class Currency {
        /**
         * Holds all the currency instances by the name.
         * Only one currency instance with the given name can exist.
         */
        private static Map<String, Currency> currencies = new HashMap<>();
        /**
         * Reference to the base currency.
         */
        private static Currency baseCurrency;

        /**
         * Name of the currency. E.g. "silver".
         */
        private String name;
        /**
         * Rate compared to base currency.
         * E.g. "silver" to "copper" is 10.
         */
        private int rate;

        /**
         * Instantiate a new currency. Can be called only from the class itself (due to private visibility).
         *
         * @param name Name of the currency.
         * @param rate Rate to the base currency.
         */
        private Currency(String name, int rate) {
            // cannot instantiate from outside
            this.name = name;
            this.rate = rate;
        }

        /**
         * {@code rate} defaults to 1.
         *
         * @see Currency#Currency(String, int)
         */
        private Currency(String name) {
            this(name, 1);
        }

        /**
         * Returns the name of the currency.
         *
         * @return The name.
         */
        public String getName() {
            return name;
        }

        /**
         * Returns the rate of the currency to the base currency.
         *
         * @return Rate to base currency.
         */
        public int getRate() {
            return rate;
        }

        /**
         * Adds a currency with the given name and rate to base currency.
         * If the currency by the name already exists, does nothing.
         * Otherwise instantiates a new Currency object and stores it in the map.
         *
         * @param currency Name of the currency.
         * @param rateToBaseCurrency Rate to base currency.
         */
        public static void add(String currency, int rateToBaseCurrency) {
            if (currencies.get(currency) != null) return; // cannot add if already exists, should throw exception?
            currencies.put(currency, new Currency(currency, rateToBaseCurrency));
            if (rateToBaseCurrency == 1) baseCurrency = currencies.get(currency);
        }

        /**
         * {@code rateToBaseCurrency} default to 1.
         *
         * @see Currency#add(String, int)
         */
        public static void add(String currency) {
            add(currency, 1);
        }

        /**
         * Returns all the currencies which are available.
         *
         * @return List of currencies.
         */
        public static List<Currency> getCurrencies() {
            return new ArrayList<Currency>(currencies.values());
        }

        /**
         * Returns the instance of a currency based on the currency name.
         * There can be only one instance of each currency.
         * Only currencies which are added using add() method will be available.
         *
         * @param currency Name of the currency.
         * @return Currency by the name, null if currency is not initialized.
         */
        public static Currency get(String currency) {
            return currencies.getOrDefault(currency, null);
        }

        /**
         * Returns the base currency. Every other currency depends on the base currency.
         *
         * @return The base currency.
         */
        public static Currency getBaseCurrency() {
            return baseCurrency;
        }

        /**
         * Returns the rate converting from the given currency to the base currency.
         *
         * @param fromCurrency Currency to convert from.
         * @return The rate.
         */
        public static int getRate(Currency fromCurrency) {
            return getRate(fromCurrency, baseCurrency);
        }

        public static int getRate(Currency fromCurrency, Currency toCurrency) {
            // TODO: compare currencies and return proper rate.
            // use the rate stored in add method
            // for example: gold -> silver should return 10
            // silver -> gold will return 0 (as the method returns int
            return 0;
        }

        /**
         * Reset the whole currency state.
         */
        public static void reset() {
            currencies = new HashMap<>();
            baseCurrency = null;
        }

        @Override
        public String toString() {
            return name;
        }
    }


Klass Coin
----------

Tähistab ühte füüsilist münti. Rahakott koosneb müntidest. Maksa saab vaid täisosaga (ühte münti ei saa pooleks teha).
Mündil on valuuta ja väärtus (näiteks 3 kulda). Põhiosas on väärtus alati 1, ehk siis mündid on kas 1 vask, 1 hõbe või 1 kuld.

Realiseeri meetodid:

- ``public Coin(int amount, Currency currency)``
- ``public Coin(Currency currency)`` - amount on 1
- ``public int getAmount()``
- ``public Currency getCurrency()``

Tuleb üle kirjutada (*override*) järgmised meetodid (``Object`` klassi omad):

- ``toString`` - tagastab sõne kujul ``%s %s``, kus esimene sõne on kogus (*amount*) ja teine on valuuta (siin ei pea ``getName()`` eraldi kutsuma, kuna ``Currency`` klassil on ``toString()`` üle kirjutatud).
  Näiteks ``1 silver`` või ``12 gold``.
- ``equals`` - tagastab ``true`` juhul, kui kogus ja valuuta on samad. Muul juhul ``false``.

Klass Price
-----------

Tähistab hinda. Hind on sisuliselt kogum valuutast ja väärtustest. Näiteks võib hind olla "2 kulda ja 3 hõbedat", aga ka "11 hõbedat ja 11 vaske"
(mis on sama mis "1 kuld, 2 hõbedat ja 1 vask" või "121 vaske").

Infot on mõistlik hoida näiteks ``HashMap``-is. Mõistlik on teha veel mingi abimeetod, mis tagastab hinna baasvaluutas (põhiosas vask).

Realiseeri meetodid:

- ``public Price(int amount, Currency currency)`` - luuakse uus objekt, millel on väärtus vastavas valuutas
- ``public Price add(int amount, Currency currency)`` - olemasolevale objektile saab juurde lisada hinda (näiteks algselt luuakse 2 silver, nüüd lisatakse 3 copper).
- ``public Map<Currency, Integer> getPrice()`` - tagastab optimaalseimal kujul hinna erinevates valuutades. See tähendab seda, et hind "123 copper" peaks siit tagasi tulema gold: 1, silver: 2, copper: 3.
  vt. ka ``toString`` meetod, mis peab sama loogikat kasutama. ``toString`` meetod võiks siis seda ``getPrice`` meetodit ära kasutada.
- ``public static Price of(int gold, int silver, int copper)`` - loob uue hinnaobjekti, kus kuld, hõbe ja vaskväärtused on vastavalt ette antud.
- ``public static Price of(int copper)`` - loob uue hinnaobjekti, kasutades vaid vase kogust. Näiteks võib mugavalt teha ``Price.of(123)`` selle asemel, et teha ``Price.of(1, 2, 3)`` või ``Price.of(0, 0, 123)``.
  Kõik need annavad sama tulemuse

Kirjutada üle järgmised meetodid:

- ``toString`` - tagastab hinna sellisel kujul, kus täidetakse ära suuremad valuutad, ülejäänud näidatakse väiksemas valuutas.
  Näiteks "123 vaske" tuleks välja näidata: "1 gold, 2 silver, 3 copper". Ehk siis "copper" väärtus ei ole kunagi üle 9 (kui näiteks oleks 11, siis 10 copperi asemel arvestada 1 silver ja 1 copper),
  "silver" väärtus ei ole kunagi üle 9.
- ``equals`` - tagastab ``true``, kui hinnad on samad. Näiteks "123 copper" on sama mis "1 gold, 2 silver, 3 copper".

Klass Purse
-----------

Reisija rahakott, sisaldab münte.

Realiseerida meetodid:

- ``public Purse(Coin... coins)``
- ``public void addCoin(Coin coin)``
- ``public List<Coin> getCoins()``
- ``public List<Coin> pay(Price price)`` - tagastab mündid, mida on vaja, et tasuda etteantud hind.
  Kui hinda ei saa tasuda, tagastab ``null``. Kui hinda saab tasuda, siis kasutatud mündid eemaldatakse rahakotist.

Klass Tavern
------------

Kõrts. Kõrtsist saab toitu osta.

Realiseerida meetodid:

- ``public void addFood(String name, Price price)`` - lisab uue toidu kõrtsi. Ühe nimega toitu on täpselt üks.
- ``public Price getPriceForFood(String name)`` - tagastab toidu hinna. Kui toitu pole, tagastab ``null``.
- ``public boolean buy(String name, Purse purse)`` - toidu ostmine. Kui selline toit on olemas ja rahakotis on piisavalt raha,
  siis tagastab ``true`` ning toit saab kõrtsist otsa. Kasutab ``Purse.pay`` meetodit. Kui toitu pole või kotis pole piisavalt raha,
  tagastab ``false``.


Boonus
======

**Currency**

Kui põhiosas oleks valuutad sellised, et "copper" puhul on ``rateToBaseCurrency`` 1, "silver" puhul 10 ja "gold" puhul 100, siis lisaosas võime kasutada ka muid kursse.
Näiteks lisame "bronze" ja rate 7. See tähendab siis seda, et ühe "bronze" eest saab 7 "copper"-it (või täpsemalt öeldes, 7 baasühikut).
Baasühiku määramiseks kasutatakse kurssi 1 (selleks on ka eraldi meetod, kus kurssi üldse ei määrata, nt: ``Currency.add("copper")``).

Kõik erinevad meetodid peavad arvesse võtma valuutasid. Ehk siis ``Coin`` peab lubama sellist valuutat. ``Price`` peab lubama ja oskama seda ka ``toString`` meetodis välja arvutada/näidata
("bronze" valuuta näite puhul näiteks 19 x copper => 1 silver, 1 bronze, 2 copper). ``Purse`` peab oskama arvutada vastava valuutaga ja seda ära kasutama, et optimaalseim tulemuse tagastada.

**Tavern**

``Tavern`` klassis on võimalik sama nimega kaupa lisada mitu tükki. Neil võib olla erinev hind. Ostmisel müüakse kõigepealt kõige odavama hinnaga toode jne.
Ehk siis kui näiteks lisatakse "pizza" hinnaga 12, hinnaga 11 ja hinnaga 12, siis esimese ostu puhul tuleb maksta 11, teise ostuga 12, kolmandaga 12. Kui kaup saab otsa, siis osta ei saa (nagu põhiosas).
``getPriceForFood`` tagastab seega kõige odavama toote hinna.

Realiseerida meetod ``public List<Coin> buyWithChange(String name, Purse purse)``, mis töötab nagu ``buy``, aga tagastab võimalikult väikese koguse münte nii, et ülemakstud summa saab tasa.
Tagasimakstud mündid lähevad rahakotti. Meetodi sees luuakse vajalikud ``Coin`` objektid.

Näiteks kui kaup maksab 12, aga maksti 20, siis tagastab 8 raha (8 x 1 copper). Kui kaup maksab 66, aga maksti 100, siis tagastab 3 x 1 silver, 4 x 1 copper.
Siin tuleb arvetada, et kõrtsis on kõiki rahaühikuid lõputult.

Tagastatavada raha puhul võite mõelda järgmise näite peale. Kui meil on rahaühikud 5, 4, 2, 1. Siis, et tagastada 8 raha, on optimaalseim tulemus 2 x 4 raha (kaks ühikut raha).
Kui võtate ahnelt, saate tulemuseks 5 + 2 + 1, mis on kokku kolm ühikut raha.

Üks võimalus on siin kasutada ära ``Purse.pay()`` meetodi sisu (vt järgmist punkti). Ainuke vahe on selles, et kui ``Purse`` objekti puhul on rahatähtede kogused piiratud, siis siin ei ole
(saate kasvõi mõelda nii, et lisate hästi palju ``Coin`` objekte ning arvutate siis optimaalseima tulemuse.

**Purse**

``pay`` meetod peab tagastama võimalikult väikese müntide arvu, kui hinda saab tasuda täpselt. Kui näiteks kotis on 2 x silver, 2 x copper ja hind on 11 copper, tuleb tagastada 1 silver, 1 copper.
Kui hinda täpselt tasuda ei saa, tuleb maksta võimalikult vähe üle hinna. Kui eelmise näite puhul hind on 3, tuleb maksta 1 silver.

Võib vaadata algoritmi näiteks siit: https://www.geeksforgeeks.org/find-minimum-number-of-coins-that-make-a-change/

Kui antud ülesande puhul on piirang see, et müntide arv ei ole lõpmatu. Üks võimalik lahendus on kirjutada rekursiivne meetod, mis proovib järjest kõik kombinatsioonid läbi sügavuti.
Selles osas on soovitus, et mündid on mõistlik eelnevalt sorteerida ning jooksvalt tasub meelde jätta hetke parim tulemus - vastasel korral läheb ajaga üle piiri.

Siin võite mõelda sarnase olukorra peale nagu ``Tavern.buyWithChange()`` eetodi juures viidatud. Kui meil on rahatähed 5, 4, 2, 1. Vaja on maksta 8 raha. Oletame, et kõiki rahasid on piisavalt.
Maksta tuleks 4 + 4.

Teine näide. Müntide väärtused on 1, 3, 8. Maksta on vaja 9 raha. Optimaalseim tulemus on 1 + 8 (kahe mündiga).

**Coin**

Lisaosas kasutame ka münte, mille väärtus ei ole 1. Näiteks ``Coin(7, Currency.get("silver"))``.

Põhiosa näide
-------------

.. code-block:: java

    // let's register currencies
    Currency.add("copper");
    Currency.add("silver", 10);
    Currency.add("gold", 100);

    // use "variables" to shorten currency usage
    Currency c = Currency.get("copper");
    Currency s = Currency.get("silver");
    Currency g = Currency.get("gold");

    Coin c1 = new Coin(s);
    System.out.println(c1.getAmount());   // 1
    System.out.println(c1.getCurrency()); // silver

    Coin c2 = new Coin(s);
    Coin c3 = new Coin(g);
    // this won't be used in purse in main part
    // but this should work
    Coin c4 = new Coin(10, g);

    System.out.println(c4.getAmount());    // 10
    System.out.println(c4.getCurrency());  // gold

    Purse purse = new Purse(c1, c2, c3);

    Price price = Price.of(1, 0, 10);
    Price price2 = Price.of(110);

    System.out.println(price.equals(price2)); // true

    Price price3 = Price.of(111);
    System.out.println(price3); // 1 gold, 1 silver, 1 copper
    System.out.println(price.equals(price3)); // false

    List<Coin> paidCoins = purse.pay(price);
    System.out.println(paidCoins); // [1 gold, 1 silver]
    System.out.println(purse.getCoins()); // [1 silver]

    Tavern tavern = new Tavern();
    tavern.addFood("pasta", Price.of(11));

    System.out.println(tavern.getPriceForFood("pasta")); // 1 silver, 1 copper

    System.out.println(tavern.buy("pasta", purse)); // false, not enough money
    purse.addCoin(new Coin(g));
    System.out.println(tavern.buy("pasta", purse)); // true
    System.out.println(purse.getCoins()); // [1 silver]

    System.out.println(tavern.getPriceForFood("pasta")); // null, no more this food

    tavern.addFood("pasta", Price.of(20));
    // let's add 2 silver coins
    purse.addCoin(new Coin(s));
    purse.addCoin(new Coin(s));
    System.out.println(purse.getCoins()); // [1 silver, 1 silver, 1 silver]
    tavern.buy("pasta", purse);
    System.out.println(purse.getCoins()); // [1 silver]

Boonusosa näited
----------------

.. code-block:: java

    // test different currencies

    Currency.reset();
    Currency.add("1 EUR"); // base
    Currency.add("2 EUR", 2);
    Currency.add("4 EUR", 4);
    Currency.add("5 EUR", 5);

    Currency cur1 = Currency.get("1 EUR");
    Currency cur2 = Currency.get("2 EUR");
    Currency cur4 = Currency.get("4 EUR");
    Currency cur5 = Currency.get("5 EUR");

    Purse purse2 = new Purse(new Coin(cur1),
            new Coin(cur2), new Coin(cur2), new Coin(cur2),
            new Coin(cur4), new Coin(cur4),
            new Coin(cur5)
            );

    Price price8 = new Price(8, cur1);
    System.out.println(purse2.pay(price8)); // [1 4 EUR, 1 4 EUR]

    // the same example, using coin amount and one currency
    Currency.reset();
    Currency.add("EUR");
    Currency eur = Currency.get("EUR");
    Purse purse3 = new Purse(new Coin(eur),
            new Coin(2, eur), new Coin(2, eur), new Coin(2, eur), new Coin(2, eur),
            new Coin(4, eur), new Coin(4, eur),
            new Coin(5, eur), new Coin(5, eur)
    );
    price8 = new Price(8, eur);
    System.out.println(purse3.pay(price8)); // [4 EUR, 4 EUR]
    System.out.println(purse3.pay(price8)); // [5 EUR, 2 EUR, 1 EUR]
    System.out.println(purse3.pay(price8)); // [5 EUR, 2 EUR, 2 EUR]
    System.out.println(purse3.pay(price8)); // null <- no more coins left
    System.out.println(purse3.getCoins()); // [2 EUR] <- one coin remaining

    // test buyWithChange
    Currency.reset();
    Currency.add("1-EUR"); // base
    Currency.add("2-EUR", 2);
    Currency.add("4-EUR", 4);
    Currency.add("5-EUR", 5);
    Currency.add("13-EUR", 13);
    Purse purseForChange = new Purse(new Coin(Currency.get("13-EUR")), new Coin(Currency.get("13-EUR")));
    Tavern tavern1ForChange = new Tavern();
    tavern1ForChange.addFood("Ice Cream", new Price(6, Currency.getBaseCurrency()));
    tavern1ForChange.addFood("Ice Cream", new Price(10, Currency.getBaseCurrency()));
    tavern1ForChange.addFood("Ice Cream", new Price(6, Currency.getBaseCurrency()));
    tavern1ForChange.addFood("Ice Cream", new Price(5, Currency.getBaseCurrency()));
    System.out.println(tavern1ForChange.getPriceForFood("Ice Cream"));  // 1 5-EUR
    System.out.println(tavern1ForChange.buyWithChange("Ice Cream", purseForChange)); // [1 4-EUR, 1 4-EUR]
    System.out.println(purseForChange.getCoins()); // [1 13-EUR, 1 4-EUR, 1 4-EUR]
    System.out.println(tavern1ForChange.getPriceForFood("Ice Cream"));  // 1 5-EUR, 1 1-EUR
    System.out.println(tavern1ForChange.buyWithChange("Ice Cream", purseForChange)); // [1 2-EUR]   <- used 4 + 4 for paying
    System.out.println("Reaining coins: " + purseForChange.getCoins()); // [1 13-EUR, 1 2-EUR]
    System.out.println(tavern1ForChange.getPriceForFood("Ice Cream"));  // 1 5-EUR, 1 1-EUR
    System.out.println(tavern1ForChange.buyWithChange("Ice Cream", purseForChange)); // [1 5-EUR, 1 2-EUR]
    System.out.println("Remaining coins: " + purseForChange.getCoins()); // [1 5-EUR, 1 2-EUR, 1 2-EUR]
    System.out.println(tavern1ForChange.buyWithChange("Ice Cream", purseForChange)); // null <- no money for ice cream 10EUR


Soovitusi tegemiseks
--------------------

1) realiseeri ``Currency`` funktsionaalsus
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Currency`` klass on suures osas ette antud. Seal on vaja realiseerida vaid üks meetod: ``public static int getRate(Currency fromCurrency, Currency toCurrency)``.
Selle eesmärk on tagastaa kurss kahe valuuta vahel.

Testida võib sellise koodiga:

.. code-block:: java

    Currency.add("copper");
    Currency.add("silver", 10);
    Currency.add("gold", 100);

    System.out.println(Currency.getRate(Currency.get("silver"), Currency.get("copper"))); // 10
    System.out.println(Currency.getRate(Currency.get("silver"), Currency.get("silver"))); // 1
    System.out.println(Currency.getRate(Currency.get("silver"), Currency.get("gold"))); // 0
    System.out.println(Currency.getRate(Currency.get("gold"), Currency.get("copper"))); // 100

2) realiseeri ``Coin``
^^^^^^^^^^^^^^^^^^^^^^

``Coin`` on sisuliselt andmeobjekt, mis sisaldab kogust ja valuutat. Põhiosas otseselt kogust ei kasutata, samas peab see funktsionaalsus olema realiseeritud.

Konstruktoris antakse ette ``amount`` ja ``Currency``, need tuleb ära salvestada.

``equals`` meetodiga tuleb vaadata, et nii ``amount`` kui ka valuuta oleks täpselt samad.
``hashCode`` meetod tagastab täisarvu. Selle peamine reegel on see, et kui ``equals`` on tõene, peavad kahe objekti räsikoodid samad olema.
Kui ``equals`` ei ole tõene, võivad need ka samad olla. Mõistlik oleks, et erinevate objektide puhul oleks räsi erinev.

Loe täpsemalt siit: https://ained.ttu.ee/javadoc/oopSpecialMethods.html#equals-ja-hashcode

Kood testimiseks:

.. code-block:: java

    Coin coin1 = new Coin(Currency.get("copper"));
    Coin coin2 = new Coin(1, Currency.get("copper"));
    System.out.println(coin1); // 1 copper
    System.out.println(coin2); // 1 copper
    System.out.println(coin1.equals(coin2)); // true
    Coin coin3 = new Coin(17, Currency.get("silver"));
    System.out.println(coin3); // 17 silver
    Coin coin4 = new Coin(170, Currency.get("copper"));
    System.out.println(coin4); // 170 copper
    System.out.println(coin4.equals(coin3)); // false
    System.out.println(coin4.equals(null)); // false

    // if equals is true, then hashcode has to be the same
    System.out.println(coin1.hashCode() == coin2.hashCode()); // true
    // the following should return false, but in certain cases can also return true
    System.out.println(coin1.hashCode() == coin3.hashCode()); // false
    System.out.println(coin3.hashCode() == coin4.hashCode()); // false

3. realiseeri ``Price``
^^^^^^^^^^^^^^^^^^^^^^^

See klass hoiab hinnainfot. Kuna hinda võib kirjeldada erinevates valuutades, võib siin olla kaks lahendust:

1. hoida hinnainfot baasvaluutas (näiteks 1 silver, 1 copper => 11 copper)
2. hoida kujutist (``Map``) valuutadest ja nende kogustest ({silver: 1, copper: 1})

Esimene variant on vist mõnevõrra lihtsam realiseerida.

``Price.of`` on abimeetod, millega saab luuba ``Price`` objekti mugavalt. Selle asemel, et kirjutada:

.. code-block:: java

    Price p1 = new Price(1, Currency.get("silver")).add(1, Currency.get("copper"));

Saab teha nii:

.. code-block:: java

    Price p1 = Price.of(0, 1, 1);

Seega ``of`` meetod võib luua uue hinnaobjekti ja lisada ``add`` meetodiga vastavalt kulla, hõbeda ja vase kogused.

``getPrice()`` meetod peab tagastama ``Map`` objekti, milles on väärtused iga erineva valuuta kohta. Ehk siis näiteks: ``{gold: 1, silver: 2, copper: 4}``.
Kui sa boonusosa ei plaani teha, siis selles meetodis võib kasutada täisarvulist jagamist ja moodulit, et jagada hind erinevate valuutade peale ära.
Eelnevas näites oli hind 124 vaske. See siis kõigepealt jagatakse kuldadeks, tulemus on 1 kuld, üle jääb 24. See jaguneb kaheks hõbedaks, üle jääb 4.

Boonusosa puhul on siin vaja kasutada ``Currency.getCurrencies()`` meetodit, mis tagastab kõik valuutad. Oluline on valuutade jagamine nende ``rate`` väärtuse järgi.

``toString()`` meetod peaks ära kasutama ``getPrice()`` tulemust. Tuleb arvestada, et ``Map`` objekt ei hoia elementide järjekorda. Seega peaks siin järjekorra peale mõtlema.

Kood testimiseks:

.. code-block:: java

    p1 = Price.of(0, 1, 1);
    System.out.println(p1.getPrice()); // {silver=1, copper=1}
    System.out.println(p1); // 1 silver, 1 copper
    p1 = Price.of(2, 3, 4);
    System.out.println(p1.getPrice()); // {silver=3, gold=2, copper=4}
    System.out.println(p1); // 2 gold, 3 silver, 4 copper
    p1 = Price.of(412);
    System.out.println(p1.getPrice()); // {silver=1, gold=4, copper=2}
    System.out.println(p1); // 4 gold, 1 silver, 2 copper

Realiseeri ``Purse``
^^^^^^^^^^^^^^^^^^^^

Objekt hoiab münte ning seda saab kasutada maksmiseks.

Konstruktorisse saab kaasa anda erineva koguse münte. Kirjapilt ``Coin... coins`` tähistab seda, et meetod võib vastu võtta 0 või rohkem arvu argumente.
Kõik argumendid on ``Coin`` tüüpi. Meetodis hiljem on muutuja ``coins`` massiiv ``Coin`` tüüpi objektidest. Sellise kirjapildi tähistamiseks kasutatakse terminit *varargs*.

Massiivi kohta võib lugeda lähemalt siit: https://ained.ttu.ee/javadoc/Array.html

Aga tavapärane kasutus konstruktoris võib välja näha järgmine:

.. code-block:: java

    for (Coin c : coins) {
        // do stuff
    }

``pay()`` meetodi realiseerimine on selle ülesande kõige keerulisem osa. Siin on võimalik minna väga keeruliseks ära.
Boonusosas vajab lahendus päris korralikku algoritmi. Põhiosa jaoks (mida see juhend katab) pole nii keerulist lahendust vaja.

Algoritm, millega peaks põhiosa läbi saama, võiks olla järgmine:

::

    hinnajääk = algne hind
    tulemus = []
    korda, kui hinnajääk > 0:
        leida suurim vaba (ehk siis ei sisaldu tulemuses) münt, mille väärtus <= hinnajääk
        kui selline münt on:
            lisada see tulemusse
            hinnajääk = hinnajääk - mündi väärtus
        kui sellist münti ei ole:
            leia kõige väiksem vaba münt
            kui selline münt on:
                lisada see tulemusse
                hinnajääk = hinnajääk - mündi väärtus
            kui sellist münti ei ole:
                järelikult ei saagi sellist hinda tasuda
                tagastada null
    kui hinnajääk <= 0:
        LISAKONTROLL (vt allpool)
        eemaldada rahakotis olevate müntide seast kõik tulemuse mündid
        tagastada tulemus
    muul juhul:
        järelikult ei saanud olemasolevatest müntidest maksta
        peame vaatama veel olukorda, kus kasutame hinnajäägist suuremat münti maksmiseks
        tagastada null

Kindlasti on võimalikud ka muud lahendused.

Mõned koodinäited:

.. code-block:: java

    // to shorten the code
    Currency c = Currency.get("copper");
    Currency s = Currency.get("silver");
    Currency g = Currency.get("gold");

    Purse purse1 = new Purse(new Coin(c), new Coin(c),
            new Coin(s), new Coin(s), new Coin(s),
            new Coin(g), new Coin(g), new Coin(g)
    );

    System.out.println(purse1.pay(Price.of(2))); // [1 copper, 1 copper]   should not be 1 silver
    System.out.println(purse1.pay(Price.of(30))); // [1 silver, 1 silver, 1 silver]   should not be 1 gold
    System.out.println(purse1.pay(Price.of(200))); // [1 gold, 1 gold]
    System.out.println(purse1.pay(Price.of(100))); // [1 gold]   should not be [1 gold, 1 gold]
    System.out.println(purse1.pay(Price.of(1))); // null   purse is empty

    // another try, a bit harder cases
    purse1 = new Purse(new Coin(c), new Coin(c),
            new Coin(s), new Coin(s), new Coin(s),
            new Coin(g), new Coin(g), new Coin(g)
    );

    System.out.println(purse1.pay(Price.of(3))); // [1 silver]     should not be [1 copper, 1 copper, 1 silver]
    System.out.println(purse1.getCoins()); // [1 copper, 1 copper, 1 silver, 1 silver, 1 gold, 1 gold, 1 gold]
    System.out.println(purse1.pay(Price.of(30))); // [1 gold]     should not be [1 silver, 1 silver, 1 gold]
    System.out.println(purse1.getCoins()); // [1 copper, 1 copper, 1 silver, 1 silver, 1 gold, 1 gold]
    System.out.println(purse1.pay(Price.of(23))); // [1 gold]     should not be [1 copper, 1 copper, 1 silver, 1 silver, 1 gold]
    System.out.println(purse1.getCoins()); // [1 copper, 1 copper, 1 silver, 1 silver, 1 gold]
    System.out.println(purse1.pay(Price.of(123))); // null
    System.out.println(purse1.getCoins()); // [1 copper, 1 copper, 1 silver, 1 silver, 1 gold]


Viimastes näidetes võib juhtuda probleem, kus näiteks rahakotiga: 1, 1, 10, 10, 10, 100, 100, 100 (kus 1 - copper, 10 - silver, 100 - gold)
on vaja maksta summat 3. Kui võtta puhtalt eelnevalt kirjeldatud algoritmi järgi, võtame alguses mündi 1 ja 1
(mõlemad väiksemad hinnajäägist). Nüüd on vaja tasuda veel summa 1, aga meil on kõige väiksem raha 10. Seega tulemus oleks: 1, 1, 10.
See aga pole väga mõistlik.

Seega, kui oleme leidnud tulemuse, siis peaks tegema ühe lisakontrolli (eelnevas algoritmist ``LISAKONTROLL`` osa):
Kui mingi tulemus on käes, proovime vaadata, kas algset hinda annab tasuda ühe rahaga. Seda on mõtet teha vaid siis,
kui me pole täpset tulemust saanud. Ehk siis umbes nii:

::

    kui hinnajääk != 0:
        ehk siis me pole täpset tulemust saanud
        leiame mündi, mille väärtus on suurem algsest hinnast
        kui selline münt on olemas, määrame selle tulemuseks
        tulemus = [leitud münt]

Veel keerulisem olukord tekib siis, kui me peame tasuma samade kupüüride korral hinda 103. Algoritmi järgi saaksime tulemuse:
100, 1, 1, 10. Kuidas nüüd sellises olukorras saavutada tulemuse 100, 10? Las see jääda teil endal välja mõelda.

Testidest moodustab vaid üks väike osa selle raskema olukorra. Ülejäänud olukorrad lahenduvad eelneva algoritmi + lisakontrolliga.

Realiseeri ``Tavern``
^^^^^^^^^^^^^^^^^^^^^

Kõrtsis on toidud ja toitudel hinnad. Toitu saab osta kasutades rahakotti.

Põhiosas on igat toidu maksimaalselt 1. Kui toit ära ostetakse, siis seda toitu enam kõrtsis ei ole.

Toidu lisamisel on mõistlik see salvestada ``Map`` objekti, kus võti on nimi (``String``) ja väärtus on hind (``Price``).
Boonusosas tuleb arvestada, et ühel toidul võib mitu hinda olla, seega peab väärtus olema omakorda mingi hulk (nt ``List`` vms) hindadest.

Toidu hinna pärimisel tagastatakse ``Map`` objektist hind, kui selline toit on olemas. Muul juhul tagastatakse ``null``.

``buy()`` meetodis tuleb kõigepealt leida toidu hind. Kui selline toit on olemas, siis kutsutakse välja ``purse.pay()``.
Kui see tagastab mündid, tuleb toit kõrtsist eemaldada ning tagastada ``true``. Kui toitu üldse pole
või kui ostmine ei õnnestu (raha pole), siis tagastada ``false``.

Koodinäide:

.. code-block:: java

    Tavern t = new Tavern();
    System.out.println(t.getPriceForFood("Pizza")); // null
    t.addFood("Pizza", Price.of(13));
    System.out.println(t.getPriceForFood("Pizza")); // 1 silver, 3 copper
    Purse purseForTavern = new Purse(new Coin(Currency.get("gold")));
    System.out.println(t.buy("Pizza", purseForTavern)); // true
    System.out.println(t.getPriceForFood("Pizza")); // null       <- oh noes, no more pizza

    t.addFood("Beer", Price.of(3)); // cheap beer
    purseForTavern = new Purse(new Coin(Currency.get("copper")));
    System.out.println(t.buy("Beer", purseForTavern)); // false   <- nooo, not enough money
    System.out.println(t.getPriceForFood("Beer")); // 3 copper    <- beer still exists

    // ok, let's fix the last problem
    purseForTavern.addCoin(new Coin(Currency.get("gold")));
    System.out.println(t.buy("Beer", purseForTavern)); // true    <- much success
    System.out.println(t.getPriceForFood("Beer")); // null

Viiteid
-------

Loe lisaks: https://ained.ttu.ee/javadoc/oop.html

``equals`` ja ``hashCode`` meetoditest: https://ained.ttu.ee/javadoc/oopSpecialMethods.html#equals-ja-hashcode
