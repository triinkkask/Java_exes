EX08 (Cookie) Clicker
=====================

Kaust Gitis: ``EX08Cookie``

Ülesande idee on saanud inspiratsiooni mängust: http://orteil.dashnet.org/cookieclicker/

Mängu kirjeldus:

- peate koguma küpsiseid
- Küpsiseid saad kasutada erinevate asjade ostmiseks
- Küpsiseid saab koguda, kui klikkida küpsise pildi peale
- Iga klikk küpsisel annab niipalju küpsiseid juurde, kui palju on mängijal kursoreid
- Mängu alguses on 1 kursor, seega üks klikk küpsisel annab ühe küpsise juurde
- Teatud küpsiste arvu eest on võimalik osta uus kursor. Kui näiteks osta teine kursor, siis iga klikiga saab juurde 2 küpsist jne
- Kursori hind suureneb iga ostuga.
- Lisaks kursorile on võimalik osta klikkereid
- Klikker hakkab teatud ajavahemiku eest mängija eest küpsisel klikkima
- Kui mängijal on näiteks 3 kursorit, siis klikker suurendab teatud aja pärast küpsiste kogust 3 võrra (justkui klikiks küpsisel)
- Mida rohkem klikkereid osta, seda väiksemaks läheb ajavahemik, mille jooksul automaatne küpsise klikk tehakse
- Klikkeri hind suureneb iga ostuga.
- Kolmas ostetav komponent on vabalt valitud objekt, mis suurendab küpsiste arvu iga sekundi tagant
- See komponent suurendab küpsiste arvu teatud arvu küpsiste võrra, kusjuures mida rohkem neid komponente on ostetud, seda rohkem küpsiste arv suureneb.

**Mängu võib realiseerida ka mingi muu objektiga, mida saab koguda ja klikkida (näiteks kohvi vms).**

Kasutajaliidese nõuded
----------------------

- Taust peab olema vähemalt kahevärviline, võib kasutada taustapilti
- Ükski nupp ei tohi olla vaikimisi stiiliga
- Kui kursor satub mingi nupu kohale, peab nupp muutuma teist värvi. Sama kehtib nupule klikkimisel.
- Mitteaktiivsetel nuppudel peab olema eraldi stiil. Sobib ka see, kui neid üldse ei näidata
- Akna suurust ei tohi saada muuta
- Komponendi (kursor, klikker, vabalt valitud komponent) arvu saab suurendada (ehk siis uue osta) vaid siis, kui selleks on piisavalt küpsiseid
- Komponendi kogus suureneb ühe võrra
- Komponendi hind ja arv peaks olema jälgitav
- Rakenduses on nupp, mis avab uue akna, milles on autori nimi, valmistamise kuupäev vms.
- Rekanduses on nupp, mis paneb kogu rakenduse/akna kinni.

Näide kasutajaliidesest (siin on kaks komponenti näha):

.. image:: https://courses.cs.ttu.ee/w/images/c/cc/ITI0011-2017-Cookie2.png
    :width: 400px

Komponentide algväärtused
-------------------------

See pole otseselt kohustuslik, aga soovituslikud väärtused:

- Kursori hind on alguses 10, suureneb 10 võrra
- 1 kursoriga annab küpsise klikk 1 küpsise, 2 kursoriga 2 küpsist jne.
- Mängu alguses on 1 kursor.
- Klikkeri hind on alguses 50, suureneb 50 võrra
- Klikker "klikib" alguses 5s tagant, iga järgmise klikkeriga väheneb see 0.1s võrra.
- Minimaalne klikkeri klikiintervall on üks sekund (ehk sellest kiiremaks ei lähe)
- Klikker suurendab "kliki" ajal küpsiste arvu vastavalt kursori olekule (ehk siis sama palju kui manuaalne klikk suurendaks)
- Kolmanda komponendi hind on alguses 200, suureneb 200 võrra.
- Kolmas komponent võiks ühe sekundi tagant suurendada küpsiste kogust, mida rohkem neid komponente on, seda rohkem küpsiseid tuleb juurde
- Näiteks 1 komponendiga 10 küpsist, kahega 20 jne.

Punktide jagunemine
-------------------

- 1p - funktsionaalsus kursori ostmiseks/kasutamiseks + visuaalsed nõuded kasutajaliidesele
- +0.5p - funktsionaalsus klikkeri + täiendava komponendi ostmiseks/kasutamiseks

vt. allpool boonusosa +0.5p

Soovitusi
---------

Selles ülesandes on kolm (erineva tüüpi) komponenti. Kuna neil kõikidel on hind, kogus ja visuaalse külje pealt nupp, mingi label jne,
siis oleks mõistlik proovida realiseerida üks oma JavaFX komponent, mida siis konkreetse komponendi jaoks laiendada.

Siin on täitsa mõistlik ülevaade, kuidas seda teha: https://docs.oracle.com/javafx/2/fxml_get_started/custom_control.htm

Näiteks teete mingi klassi, mis laiendab ``VBox`` klassi. Oluline on jälgida, et kui kasutate FXML-i, siis sealne juurelement (ehk kõige välimine element)
oleks ``fx:root``. Sinna klassi on mõistlik lisada mingi meetod, mis käivitatakse peale nupu "Buy" ostmist (kuna see on neil natuke erinev).
Samas kogu see funktsionaalsus, mis kontrollib, kas küpsiseid on, kas nuppu peaks näitama jne võib olla ülemklassis.


Boonusosa
---------

Realiseeri näiteks üks järgmistest:

- küpsisel käsitsi klikkides tuleb info, mis näitab, mitu küpsist lisandus (+3 näiteks).
  See küpsiste lisandmuse info peaks sujuvalt liikuma ekraani ülaosasse (nagu originaalses küpsisemängus)
- küpsise all on piimanivoo (nagu originaalmängus), mis lainetab. Nivoo näitab ühtlasi kordajat, millega saadud küpsiste kogused läbi korrutatakse
- mõni muu vabalt valitud mänguelement originaalmängust (või kasvõi mõni uus idee) - see tuleks õppejõuga läbi arutada

Boonusosa soovitusi
-------------------

Küpsisel klikkimisel võib lisada ühe ``Label`` objekti, mis animatsioniga (``Timeline``) liigub üles. Ühtlasi võiks ta vaikselt läbipaistvaks muutuda.
Iga klikkiga tehakse eraldi selline ``Label``.

Nivoo tegemiseks võib proovida kasutada näiteks ``sin`` võrrandit. Võib teha ka nii, et panete kitsad ristkülikud erineva kõrgusega üksteise kõrvale:

::

    ##    ###    #
    ###  #####  ##
    ##############
    ##############
    ##############

Mõistlik oleks siis teha nii, et saate määrata: kõrguse, perioodi (millise laiuse tagant kordub), kogu laiuse.