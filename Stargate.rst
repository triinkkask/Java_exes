Startgate
=========

**NB! Ülesande lahenduses ei tohi olla ühtegi for loopi. Kõik peab olema lahendatud Stream API'ga.**

``Kaust Gitis: EX10Stargate``

``Pakk Gitis: ee.ttu.iti0202.stargate``

Tähevärav (Stargate) on ringikujuline seade, mis loob "ussiaugu" kahe tähevärava vahel ja võimaldab läbida pikki vahemaid väga lühikese ajaga. Teise täheväravaga ühenduse loomiseks on üldiselt vajalik valimisseade DHD (Dial Home Device).
Sulle on antud andmed planeetide kohta failis "planets_data.csv". Sinu ülesandeks on neid andmeid töödelda kasutades Java Stream API-d, et luua vastavad objektid ja kätte saada nõutud info.

class Planet
------------

``Pakk Gitis: ee.ttu.iti0202.stargate.planet``

Meetodid:

- ``public Planet(String name, long inhabitants, boolean stargateAvailable, boolean dhdAvailable, List<String> teamsVisited)`` - konstruktor

- ``public String getName()`` - tagastab planeedi nime

- ``public long getInhabitants()`` - tagastab planeedil olevate elanike arvu

- ``public boolean isStargateAvailable()`` - tagastab true, kui planeedi tähevärav on töökorras, muul juhul false

- ``public boolean isDhdAvailable()`` - tagatab true, kui DHD (Dial Home Device) on töökorras, muul juhul false

- ``public List<String> getTeamsVisited()`` - tagastab listi planeeti külastanud tiimidest

- ``public String toString()`` - tagastab planeedi nime

Ära loo neid gettereid, konstruktorit käsitsi.
Kui privaatsed muutujad on loodud, siis IntelliJ menüüst **Code | Generate** või **Alt+Insert**/**⌘N**.

https://www.jetbrains.com/help/idea/generating-code.html

class PlanetBuilder
-------------------
``Pakk Gitis: ee.ttu.iti0202.stargate.planet``

Kasuta planeetide loomiseks builder patternit.

PlanetBuilder klassi suudab IntelliJ automaatselt genereerida. Planet klassi konstruktoril -> "Refactor" -> "Replace Constructor with Builder..." -> **Eemalda set prefiksid** -> "Refactor"

Meetodid peavad olema järmisel kujul: ``public PlanetBuilder name(String name)`` (mitte ``setName``).

Meetod, mis loob objekti, peab olema nimega ``public Planet createPlanet()`` (mitte ``build()``).

Builder pattern: https://jlordiales.me/2012/12/13/the-builder-pattern-in-practice/

class Space
-----------
``Pakk Gitis: ee.ttu.iti0202.stargate.space``

Loo iga planeedi kohta Planet objekt ja pane need listi. Planeedid loe sisse failist "planets_data.csv".
Sisselugemiseks ja töötlemiseks kasuta Files API voogu (stream).

Kõigepealt loo meetod:

- ``public List<Planet> csvDataToPlanets(String filePath)`` - loob etteantud faili (nt "planets_data.csv") andmete põhjal Planet
  objektidest koosneva listi, kus iga objekt
  on infoga täidetud ehk eelnevalt mainitud vastavad getterid tagastavad failis oleva asjakohase info. Planeedi objekti loomisel
  kasuta *Builder* disaini mustrit.

Seejärel andes eelneva meetodi tulemuse järgnevate meetodite sisendiks.

- ``public Set<Planet> getDeadPlanets(List<Planet> planets)`` - tagastab kõik surnud planeedid.
  Surnud planeediks loetakse planeeti, kus on elanike arv 0.

- ``public Set<Planet> needToVisit(List<Planet> planets)`` - tagastab planeedid, mis vajavad külastamist.
  Külastamist vajab planeet, mida pole külastanud veel ükski tiim ja planeedil on olemas töökorras tähevärav ja DHD.

- ``public OptionalDouble getAvgInhabitantsOnPlanetsWithStargate(List<Planet> planets)`` - tagasta b täheväravaga planeetide
  keskmise elanike arvu.

**Boonus:**

- ``public List<String> getTeamsWhoHaveVisitedSmallNotDeadPlanets(List<Planet> planets)`` - tagastab listi tiimidest, kes on külastanud
  väikseid (elanike arv < 2500), aga mitte surnud planeete (elanike arv > 0). Tagastatav list peaks olema tiimi numbrite
  järgi kasvavalt sorteeritult ja ei tohiks sisaldada duplikaate.

- ``public Map<String, Long> getCodeNameClassifierFrequency(List<Planet> planets)`` - tagastab
  sõnastiku, kus on igale klassifikaatorile on vastavuses selle esinemise arv.
  Klassifikaatoriks on PXX-NNN stiilis planeetide nimedest esimene osa ehk PXX.

  Näiteks PZX-123, PZX-555, PZZ-777 -> {PZX=2, PZZ=1}

- Kõik meetodid on lahendatud ühe streamiga (streami sees võib olla veel streame).

------

**Näide:**

.. code-block:: java

    public static void main(String[] args) throws IOException {

        Space space = new Space();
        String filePath = "EX10Stargate/src/ee/ttu/iti0202/stargate/space/planets_data.csv";
        List<Planet> planets = space.csvDataToPlanets(filePath);

        System.out.println(space.getDeadPlanets(planets).size());  // 70

        OptionalDouble avg = space.getAvgInhabitantsOnPlanetsWithStargate(planets);
        if (avg.isPresent()) {
            System.out.printf("Avg: %.0f\n", avg.getAsDouble());  // Avg: 186978984
        }

        System.out.println(space.needToVisit(planets));
        /*
        [Dakara, Earth, Gadmeer homeworld, Latona, M6R-867, Orin's planet, P3A-194,
         P3K-447, P3L-997, P3X-584, P4M-399, P4X-636, P8X-412, Retalia, Sartorus, Sudaria]
        */

        System.out.println(space.getCodeNameClassifierFrequency(planets).size());  // 39

        System.out.println(space.getTeamsWhoHaveVisitedSmallNotDeadPlanets(planets));  // [sg-1, sg-4, sg-9, sg-15]
    }


----------------

Streamid:

https://ained.ttu.ee/javadoc/stream.html

http://winterbe.com/posts/2014/07/31/java8-stream-tutorial-examples/

Võrdluseks tavalise tsükliga:

https://ained.ttu.ee/javadoc/python/loop.html
