Justice
=======

Kaust Gitis: ``EX14Justice``

Pakk Gitis: ``ee.ttu.iti0202.justice``

Selles ülesandes automaatteste ei ole (küll saate emaili mingi teatega) ja te peate ise välja mõtlema struktuuri. Ülesande hinne selgub õppejõule kaitsmise hetkel.

**Lugege terve ülesanne läbi enne kui tegema hakkate!**

Hindamine
---------

Hindamisel pöörame tähelepanu järgmistele asjadele:

* Üldine struktuur (klassideks jagamine, klasside omavahelised seosed, klassid on võimalikud sõltumatud)
* Klassid on loogiliselt jagatud pakkidesse ning nähtavused on võimalikult madalamad. (Ükski muutuja (peale konstantide) ei tohi public olla!)
* Muutujate, meetodite, klasside ja pakkide nimed on korrektsed.
* Objektide alamtüübid on realiseeritud polümorfselt.
* Kasutate erinevaid mustreid. *(Strategy pattern, static factory, builder pattern jms)*
* Tsüklite asemel on vood (``stream``).
* Teie meetodid ei tagasta ``null``'i. Kui tagastatav väärtus võib puududa, siis tagastate ``Optional``'i.
* Kasutate enda defineeritud erindeid. (``exception``)

**Vältige staatilisi muutujaid ja meetodeid!**

**Dependency injection:** https://stackoverflow.com/a/130862 

Ülesanne
--------

Seekord mängime kohtumängu.

1. Kohtumängu ei saa mängida, kui ei ole inimesi. Tehke klass, mis defineerib elanikku. Elaniku kohta peab olema võimalik teada saada tema ees- ja perekonnanime ning vanust.
   Kõik elanikud peavad olema registreeritud politseijaoskonnas.

2. Politseijaoskond - võimuorgan, kelle töö on inimesi süüdistada. Politseijaoskonnal peab olema meetod, mis valib suvalise **täisealise** inimese nende hulgast, kes on jaoskonnas registreeritud ja kes ei ole veel süüdistatud, valmistab hagi (``Lawsuit``) tema vastu ja edastab selle kohtusse. Hagi ei saa esitada kohtuniku või advokaadi vastu.

**Random:** https://docs.oracle.com/javase/8/docs/api/java/util/Random.html

3. Kindlasti on vaja igale hagile määrata põhjus ja aeg, millal ta esitati. Võimalikud kuriteod: mõrv, vargus, vägivald, hirmutus. Võite kindlasti midagi juurde lisada. Kuriteost sõltub trahvi suurus ja/või mitmeks päevaks elanik vangi pannakse.

**LocalDateTime:** https://docs.oracle.com/javase/8/docs/api/java/time/LocalDateTime.html

4. Igal kuriteol on minimaalne trahvi suurus ja aeg päevades. Kuritegusid on soovitatav realiseerida enumiga. *(Vihje: enum'is võib olla konstruktor ja meetodid.)*

**Näide (2.1  Constructor, Member Variables and Methods):** https://www.ntu.edu.sg/home/ehchua/programming/java/JavaEnum.html

5. Kohus. Kui kohtusse esitatakse hagi, siis kohus alustab kohtuprotsessi (``Litigation``). Kohtuprotsessile määratakse suvaline kohtunik ja suvaline advokaat. 

6. Kohtunik - inimene, kes osaleb protsessis ja teeb otsuse, kas inimene on süüdi või mitte. Kohtunikul peab olema ka nimi, vanus (30+) ja ta peab olema politseijaoskonnas registreeritud. Ehk teiste sõnadega, kohtunik on ka elanik.

7. Advokaat - inimene, kes osaleb protsessis ja kellest ka sõltub kohtuniku otsus. Advokaadi kohta peab olema võimalik teada saada, mitmes protsessis on ta juba osalenud ja mitu neist ta on võitnud (otsus tehti süüdistatava kasuks). Advokaadil peab olema ka nimi, vanus (25+) ja ta peab olema politseijaoskonnas registreeritud. Ehk teiste sõnadega, advokaat on ka elanik.

8. Kui elanik mõisteti vangi, siis ta langeb mängust välja ning politseijaoskond ei saa enam teda süüdistada.

9. Otsus võib olla kas õigeksmõistev (``Acquital``) või süüdimõistev (``Conviction``). Otsuse jaoks on mõistlik luua eraldi objekt, kuna otsus sisaldab ka trahvi ja aega, mitu päeva peab elanik vanglas istuma. 

Võimalik realisatsioon (``Judgement`` on abstraktne):

.. image:: https://i.imgur.com/aXAagQI.png

10. Igal kohtunikul peab olema strateegia, kuidas ta teeb otsuseid (võib määrata tavalise setteriga). Mõni kohtunik näiteks ei süüdista inimesi vanuses 20-30. Teine aga annab kõigile maksimaalse karistuse. See osa peab olema realiseeritud kasutades strateegia mustrit (``Strategy pattern``).

**Strategy pattern:** https://www.tutorialspoint.com/design_pattern/strategy_pattern.htm

11. Realiseerige vähemalt 3 erinevat strateegiat, kusjuures kahks neist klassidena ja üks neist lambdana (juba Main klasssis). Üks strateegia peab arvestama advokaadi võidetud kohtuprotsesside arvu ja teine peab arvestama süüdistatava vanust. Mida peab kolmas strateegia arvestama - mõelge välja ise.

12. Kui otsus on süüdimõistev, siis karistus ei pruugi olla minimaalne. Mõni kohtunik võib anda suurema karistuse.

13. Arhiiv. Kui kohtuprotsess on lõppenud ning otsus on tehtud, siis see arhiveeritakse. Arhiivil peab olema ülevaade kõikidest lõppenud kohtuprotsessidest, mis on seotud konkreetse inimesega. Samuti peab olema võimalik arhiivist teada saada, kas mõni inimene on ikka veel vabaduses (ta ei ole osalenud üheski kohtuprotsessis või ta ei istu vanglas.)

14. Tehke Main klassis näide töötavast rakendusest. Peab olema vähemalt 1 kohus, 1 politseijaoskond, 100 tavalist elanikku, 5 kohtunikut, 10 advokaati ja 50 hagi.

