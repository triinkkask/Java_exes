Miinikits Miina
===============

Kaust Gitis: ``EX10Goat``

Miinikits Miina omanikule, Madisele, on otsustatud teha tempu. Tembu tegijaks on Jaanus, Madise vana rivaal kartulikasvatuses. Miinikits Miina peab päästma madise kartulid Jaanuse miinide eest!


Põhiosa (1p)
------------

Jaanuse miinid on katki, ja ei plahvata.
Realiseeri rakendus, kus on järgnevad elemendid:

- Kits (tuleb ise pilt joonistada), kes on juhitav nooleklahvidega, st vajutades üles liigub kits üles jne.
- Kits vaatab selles suunas, kuhu liigub. Kui kits liigub paremale, vaatab ta paremale, kui liigub vasakule, vaatab vasakule jne (näiteks pöörate ja/või peegeldate pilti).
- Miinid (tuleb ise pilt joonistada), mis tekivad iga 5 sekundi tagant suvalisse kohta ekraanil (miin ei tohi tekkida nähtavast alast välja ega ka n-ö ääre peale).
- Skoori tekst, mis näitab, mitu miini on üles korjatud.
- Kits liigub väikese sammuga (kasvõi 1px). See ei ole sobiv, et on ruudustik, kus kits liigub suure sammuga ühest ruudust teise. Samamoodi miinid ilmuvad suvalisele positsioonile (mitte ruudustikku).
- Mängul peab olema "esileht", millel on kasvõi üks nupp "Mängi". Peale nupu vajutust alustatakse mänguga.

Kusjuures kui kits läheb miini vastu, siis "korjatakse" miin üles, st eemaldatakse maailmast, ja lisatakse skoorile üks punkt.

Lisaosa (1p)
------------

Oi ei, Jaanus on oma miinid korda saanud!

Tuleb realiseerida kõik baasosas, pluss järgnev:

-    Miinid plahvatavad (tuleb ise pilt joonistada) 3 sekundit pärast nende tekkimist
-    Plahvatus peab olema suurem kui miin. St, kui kits on miini lähedal, võib ta ka sellega pihta saada.
-    Kui Miina saab plahvatusega pihta, läheb talt üks elu maha.
-    Kui Miinal saavad elud otsa (tal on algul 3 elu, ehk pärast kolme plahvatust), siis mäng lõppeb. See tähendab, et näidatakse jälle "esilehte", kust võib uut mängu alustada.
-    Miinid tekivad aina tihemini, mida kauem mäng on käinud, st algul iga viie sekundi tagant, hiljem juba näiteks kolme.
-    Miini üleskorjamisel saab punkte seda rohkem, mida varem miin üles korjata. Alguses saab 5p, 1s pärast 4p jne.
