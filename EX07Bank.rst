Bank
====

Kaust Gitis: ``EX07Bank``

Kirjutada JavaFX rakendus, milles saab hoida pangakaartide infot.

Pangas on kahte tüüpi kaarte: deebet- ja krediitkaart. Igal pangakaardil on number (16-kohaline arv, mõistlik hoida sõnena) ja omanik (sõne).
Krediitkaardil on lisaks ka krediidilimiit.

Rakendus peab näitama välja kõiki panga kaartide nimekirja (selles järjekorras, kuidas need on lisatud). Kaarte peab saama lisada.
Arvestada tuleb, et deebetkaardil ei ole krediidilimiiti. Samas võiks lisamise vorm olla realiseeritud selliselt, et ei ole kaks eraldi vormi kahe erineva kaardi jaoks.
Pigem kasutada ära ühte vormi. Võimalus on limiidi välja mitte näidata, kui tüüp ei ole krediitkaart.

Kaartide nimekirja võib näidata näiteks ``TableView`` objektina, kuid võib kasutada ka midagi lihtsamat, kasvõi tekstina ``Label``'i sees.

Ülesandes te **ei pea valideerima väljade sisu** (ehk siis igasugune pangakaardi number ja omanik on korrektsed). Krediidilimiit on alati mittenegatiivne täisarv.

Boonusosa
---------

Realiseerida kaartide nimekiri ``TableView`` objektina.
Tabelis peab saama omanikku muuta, kui vastaval tabelilahtril topeltklikk teha (seda funktsionaalsust võimaldab TableView).
Krediidilimiiti saab muuta vaid krediitkaartidel.

Krediidilimiidi välja puhul oleks mõistlik teha nii, et kui reas ei ole krediitkaart, siis üldse ei võimalda selle muutmist.

Natuke vihjeid võib saada siit: https://stackoverflow.com/a/27915420/122128

Näiteks on üks võimalus laiendada klassi ``TextFieldTableCell``, milles kirjutada üle meetod ``startEdit``. Selles meetodis saab küsida aktiivse rea objekti:

.. code-block:: java

    TableRow<BankCard> tableRow = getTableRow();
    BankCard bc = tableRow.getItem();

Kus ``BankCard`` selles näites on üldine pangakaart. Edasi saab juba küsida, kui ``bc`` instants on krediitkaart, siis kutsu välja ``super.startEdit()``.
Vastasel juhul ei pea midagi tegema (ehk siis kutset ülemklassi poole ei toimu, seega meie ``startEdit`` ei tee midagi), st et lahtrisse ei looda tekstivälja.

Viiteid
-------

https://ained.ttu.ee/javadoc/javafx.html

Väga hea step-by-step õpetus, kuidas teha JavaFX rakendust:

http://code.makery.ch/library/javafx-8-tutorial/

https://docs.oracle.com/javase/8/javafx/api/javafx/scene/control/TableView.html

https://docs.oracle.com/javase/8/javafx/user-interface-tutorial/table-view.htm

https://docs.oracle.com/javafx/2/fxml_get_started/fxml_tutorial_intermediate.htm (TableView FXML-ina, mis on mõnevõrra keerulisem ja ei võimalda kõike teha)

